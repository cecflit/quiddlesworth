ack_dialogue0 <- [
	["[TUX] Good morning. I'm Tux, I came from Icy Island earlier today, and I would like to help with the current events.", "beige"],
	["[OLD SQUIRREL] Good morning, Tux. I am Ack, and I have lived in Quiddlesworth for over 10 years. I appreciate your courage and willingness to help, but I'm afraid there isn't much you can do. This ancient curse was predicted for centuries, and although not everyone believed it, all doubt is now gone. I'm old and don't have much longer to live, but I fear for the young of this forest.", "darkgreen"],
	["[TUX] I'm so sorry to hear that, Ack. But why don't they just leave? I understand that there is some attachment to this town, but lives are at stake here!", "beige"],
	["[ACK] According to the records, this curse affects all residents, no matter how far they travel, and if they do travel, they will bring the curse with them. I'm afraid there's nothing anyone can do.", "darkgreen"],
	["[TUX] Can the curse also affect me?", "beige"],
	["[ACK] You haven't been here for long enough to carry it, however, I urge you to leave as soon as possible, since there is no telling when you can be infected, too.", "darkgreen"],
	["[TUX] Do the ancient records not mention a countercurse?", "beige"],
	["[ACK] I am afraid not. All we know is that the curse was unleashed by a dark wizard named Bog that nobody knew how to defeat. Those were dark times, Tux, very dark times. Locals couldn't breathe freely until he died of old age. Most hoped his curse died with him, but as we can see now, that, alas, isn't the case.", "darkgreen"],
	["[TUX] I'm terribly sorry to hear this fate, Ack. I wish you all the best.", "beige"],
	["[ACK] To you too, Tux. Take care.", "darkgreen"]
];

ack_dialogue1 <- [
	["[TUX] Can you remind me about the curse?", "beige"],
	["[ACK] The curse was predicted aeons ago, after a dark wizard named Bog died, locals suspected that he unleashed a curse upon Quiddlesworth that nobody would be able to lift. For centuries, locals lived in fear, not knowing when the curse could start to act, until it happened last night. The forest started dying and turning purple, the temperature drastically dropped, and everybody felt stress and anxiety. I'm afraid there's nothing that can be done to stop this curse.", "darkgreen"]
];

chestie_dialogue0 <- [
	["[TUX] Hello, I am Tux. I come from Icy Island.", "beige"],
	["[YOUNG SQUIRREL] Hi Tux, my name's Chestie, I go to the local school, but it's locked now. I think you should probably leave, it's not safe here at all.", "pink"],
	["[TUX] I know, that's why I'm here. I want to try to help.", "beige"],
	["[CHESTIE] That's most noble of you, but everybody seems to think that nothing can be done to stop the curse.", "pink"],
	["[TUX] I know no such word as \"nothing\". I will try my best.", "beige"],
	["[CHESTIE] That's very nice of you, Tux. Perhaps you should talk to the adults, they may be able to help you.", "pink"],
	["[TUX] I will. There must be something that I can do. All the best, Chestie.", "beige"],
	["[CHESTIE] I believe in you. Good luck!", "pink"]
];

chestie_dialogue1 <- "I can't help you, Tux, I'm only 4 months old. Go talk to the adults.";
