cooked_fish <- Stage.StageItem("Cooked fish", "/levels/quiddlesworth/images/cooked_fish.png", "Still warm and smells delicious.");

cook_fish <- function() {
	Stage.collect_item(cooked_fish);
	Stage.update_status("Fish cooked");
	Stage.live_caption("I'm sure the locals won't mind if I use their fire.", "beige", "middle", 5);
}
