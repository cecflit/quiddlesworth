gob_dialogue0 <- [
	["[TUX] Hi, I'm Tux. I wonder if I could help with the current events here in Quiddlesworth?", "beige"],
	["[EYE-PATCHED SQUIRREL] Huh, an adventurer, eh? I'm Gob, and I'll give you a piece of advice - let it go and leave the island while you can.", "violet"],
	["[TUX] Surely there is something I can do.", "beige"],
	["[GOB] There isn't. Leave me alone, please.", "violet"]
];

gob_dialogue1 <- [
	["[TUX] You're Gob, right? I'm Tux. Sorry to bother, but the theatre manager sends me.", "beige"],
	["[GOB] The manager? What could he possibly want?", "violet"],
	["[TUX] He says you've been to the cave several times, and that you could give me some advice.", "beige"],
	["[GOB] Are you going there or something?", "violet"],
	["[TUX] Yes, actually. The mayor gave me permission and I have the key.", "beige"],
	["[GOB] *sigh* Alright then. I'll tell you this. The cave complex is quite large and can be confusing at times. There are certain unique caverns in there that you can use for navigation, additionally, you should make yourself a map. Take a pen and a piece of paper and just mark where you've been and what you found there. If you don't do this, there's a good chance you will get lost. I would give you a guide book with a map, but the last expedition team took my last copy.", "violet"],
	["[TUX] Thank you, this is very helpful!", "beige"]
];

gob_dialogue2 <- [
	["[TUX] Sorry to bother, but the theatre manager sends me.", "beige"],
	["[GOB] The manager? What could he possibly want?", "violet"],
	["[TUX] He says you've been to the cave several times, and that you could give me some advice.", "beige"],
	["[GOB] Are you going there or something?", "violet"],
	["[TUX] Yes, actually. The mayor gave me permission and I have the key.", "beige"],
	["[GOB] *sigh* Alright then. I'll tell you this. The cave complex is quite large and can be confusing at times. There are certain unique caverns in there that you can use for navigation, additionally, you should make yourself a map. Take a pen and a piece of paper and just mark where you've been and what you found there. If you don't do this, there's a good chance you will get lost. I would give you a guide book with a map, but the last expedition team took my last copy.", "violet"],
	["[TUX] Thank you, this is very helpful!", "beige"]
];

talk_to_gob <- function() {
	if(!Level.key_given) {
		if(!("gob_talked" in sector)) {
			sector.gob_talked <- true;
			sector.Tux.set_dir(false);
			Stage.dialogue(gob_dialogue0);
			Level.know_gob <- true;
			Stage.set_screen_name("Gob's house", "grey");
		} else Stage.live_caption("Don't bother, really.", "violet", "middle", 1.833);
	} else if(!Level.map_given) {
		Level.map_given <- true;
		sector.Tux.set_dir(false);
		if(!("gob_talked" in sector)) Stage.dialogue(gob_dialogue1);
		else Stage.dialogue(gob_dialogue2);
	} else Stage.live_caption("I can't do more for you, Tux.", "violet", "middle", 2.33);
}
