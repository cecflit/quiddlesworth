crowbar_reaction <- function() {
	play_sound("/sounds/metal_hit.ogg");
	Stage.replace_in_inventory("Crowbar", hook);
	sector.lock.set_visible(false);
	sector.fake_door.set_visible(false);
	sector.settings.add_object("door", "mrizelol", 764, 675, "", "(sprite \"/levels/quiddlesworth/images/bardoor/bardoor.sprite\") (script \"Stage.door(\\\"prison\\\", \\\"compromised_cell\\\")\")");
	Stage.live_caption("I broke the lock, but also my jemmy.", "beige", "middle", 4.37);
	sector.lock_broken <- true;
	Stage.clear_accepted_items();
}

rod_reaction <- function() {
	play_sound("/sounds/metal_hit.ogg");
	Stage.replace_in_inventory("Metal rod", bent_rod);
	sector.lock.set_visible(false);
	sector.fake_door.set_visible(false);
	sector.settings.add_object("door", "mrizelol", 764, 675, "", "(sprite \"/levels/quiddlesworth/images/bardoor/bardoor.sprite\") (script \"Stage.door(\\\"prison\\\", \\\"compromised_cell\\\")\")");
	Stage.live_caption("I broke the lock, but also the metal rod.", "beige", "middle", 4.37);
	sector.lock_broken <- true;
	Stage.clear_accepted_items();
}

//add_object(string class_name, string name, int posX, int posY, string direction, string data)
