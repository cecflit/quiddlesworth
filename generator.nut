rod_reaction <- function() {
	sector.generator_fixed <- true;
	electricity.set_visible(true);
	rod_so.set_visible(true);
	Stage.live_caption("There. I fixed the generator.", "beige", "middle", 1.66);
}

horseshoe_reaction <- function() {
	Stage.replace_in_inventory("Horseshoe", magnet);
	Stage.live_caption("Now I have a powerful magnet.", "beige", "middle", 2.25);
	Stage.update_status("Magnet created");
}
