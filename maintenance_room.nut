//8 = up; 4 = down; 2 = left; 1 = right

if(!("gameboard" in sector)) {
	sector.gameboard <- array(16);
	for(i <- 0; i < 16; ++i) {
		sector.gameboard[i] = array(16);
		for(j <- 0; j < 16; ++j) sector.gameboard[i][j] = 0;
	}
	cur_x <- 0;
	cur_y <- 0;
	next_dir <- 2;
	sector.gameboard[0][0] = sector.gameboard[0][0] | 2;
	while(true) {
		switch(next_dir) {
			case 8: next_dir = 4; break;
			case 4: next_dir = 8; break;
			case 2: next_dir = 1; break;
			case 1: next_dir = 2; break;
		}
		sector.gameboard[cur_x][cur_y] = sector.gameboard[cur_x][cur_y] | next_dir;
		if(cur_x == 15 && cur_y == 15) {
			sector.gameboard[15][15] = sector.gameboard[15][15] | 1;
			break;
		}
		ox <- cur_x;
		oy <- cur_y;
		do {
			next_dir = rand() & 7;
			if(cur_x < 0) cur_x = 0;
			if(cur_x > 15) cur_x = 15;
			if(cur_y < 0) cur_y = 0;
			if(cur_y > 15) cur_y = 15;
			switch(next_dir) {
				case 0: case 1: case 2: next_dir = 4; ++cur_y; break;
				case 3: case 4: case 5: next_dir = 1; ++cur_x; break;
				case 6: next_dir = 8; --cur_y; break;
				case 7: next_dir = 2; --cur_x; break;
			}
		} while(cur_x < 0 || cur_x > 15 || cur_y < 0 || cur_y > 15);
		sector.gameboard[ox][oy] = sector.gameboard[ox][oy] | next_dir;
	}
	for(i <- 0; i < 16; ++i) {
		for(j <- 0; j < 16; ++j) {
			if(sector.gameboard[i][j]) {
				rn <- rand() & 3;
				for(k <- 0; k < rn; ++k) {
					new_orientation <- 0;
					if(sector.gameboard[i][j] & 8) new_orientation = new_orientation | 1;
					if(sector.gameboard[i][j] & 4) new_orientation = new_orientation | 2;
					if(sector.gameboard[i][j] & 2) new_orientation = new_orientation | 8;
					if(sector.gameboard[i][j] & 1) new_orientation = new_orientation | 4;
					sector.gameboard[i][j] = new_orientation;
				}
			} else sector.gameboard[i][j] = rand() & 15;
		}
	}
	sector.select_x <- 0;
	sector.select_y <- 0;
	sector.game_done <- false;
}

sector.infotext <- TextObject();
sector.infotext.set_text(_("Press ACTION to close"));
sector.infotext.set_anchor_point(ANCHOR_TOP_LEFT);
sector.infotext.set_text_color(1, 1, 0, 1);
sector.infotext.set_back_fill_color(0, 0, 0, 0);
sector.infotext.set_front_fill_color(0, 0, 0, 0);
sector.infotext.set_pos(10, 18);

function check_from_pos(px, py) {
	if(sector.check_matrix[px][py]) return false;
	sector.check_matrix[px][py] = true;
	if(px == 15 && py == 15) return true;
	local ret = false;
	if(sector.gameboard[px][py] & 8 && py && sector.gameboard[px][py - 1] & 4 && check_from_pos(px, py - 1)) ret = true;
	if(sector.gameboard[px][py] & 2 && px && sector.gameboard[px - 1][py] & 1 && check_from_pos(px - 1, py)) ret = true;
	if(sector.gameboard[px][py] & 1 && px < 15 && sector.gameboard[px + 1][py] & 2 && check_from_pos(px + 1, py)) ret = true;
	if(sector.gameboard[px][py] & 4 && py < 15 && sector.gameboard[px][py + 1] & 8 && check_from_pos(px, py + 1)) ret = true;
	return ret;
}

function check_win() {
	if(!(sector.gameboard[0][0] & 2) || !(sector.gameboard[15][15] & 1)) return false;
	check_x <- 0;
	check_y <- 0;
	sector.check_matrix <- array(16);
	for(i <- 0; i < 16; ++i) {
		sector.check_matrix[i] = array(16);
		for(j <- 0; j < 16; ++j) sector.check_matrix[i][j] = false;
	}
	return check_from_pos(0, 0);
}

function win() {
	play_sound("sounds/invincible_start.ogg");
	game_selector.fade_out(0.5);
	sector.game_done <- true;
	thumb.set_visible(false);
	legs.fade(0, 0);
	for(i <- 0; i < 16; ++i) for(j <- 0; j < 16; ++j) if(!sector.check_matrix[i][j]) sector.game_tiles[i][j].fade_out(1);
	wait(3.6);
	game_bg.set_visible(false);
	for(i <- 0; i < 16; ++i) for(j <- 0; j < 16; ++j) sector.game_tiles[i][j].set_visible(false);
	sector.infotext.set_visible(false);
	sector.Tux.activate();
	Stage.allow_inventory(true);
	if(Level.data.phosphorus) {
		Stage.live_caption("There was a stick stuck in the plumbing!", "beige", "right", 3.75);
		Stage.spawn_item(stick, 1296, 416);
	} else {
		Stage.live_caption("There was a match stuck in the plumbing!", "beige", "right", 3.75);
		Stage.spawn_item(match, 1296, 416);
	}
}

function play() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);

	game_bg <- FloatingImage("/levels/quiddlesworth/images/pipes/background.png");
	game_bg.set_layer(940);
	game_bg.set_visible(true);

	sector.game_tiles <- array(16);
	for(i <- 0; i < 16; ++i) {
		sector.game_tiles[i] = array(16);
		for(j <- 0; j < 16; ++j) {
			sector.game_tiles[i][j] = FloatingImage("/levels/quiddlesworth/images/pipes/" + sector.gameboard[i][j] + ".png");
			sector.game_tiles[i][j].set_layer(945);
			sector.game_tiles[i][j].set_pos(-240 + 32 * i, -240 + 32 * j);
			if(sector.gameboard[i][j]) sector.game_tiles[i][j].set_visible(true);
		}
	}

	game_selector <- FloatingImage("/levels/quiddlesworth/images/pipes/selector.png");
	game_selector.set_layer(960);
	game_selector.set_pos(-240 + 32 * sector.select_x, -240 + 32 * sector.select_y);
	game_selector.set_visible(true);

	sector.infotext.set_visible(true);

	while(sector.Tux.get_input_held("up")) wait(0.01);

	while(true) {
		wait(0);
		if(sector.Tux.get_input_held("action")) {
			game_selector.set_visible(false);
			game_bg.set_visible(false);
			sector.infotext.set_visible(false);
			for(i <- 0; i < 16; ++i) for(j <- 0; j < 16; ++j) sector.game_tiles[i][j].set_visible(false);
			sector.Tux.activate();
			Stage.allow_inventory(true);
			return;
		} else if(sector.Tux.get_input_held("left")) {
			if(sector.select_x) {
				--sector.select_x;
				for(i <- 0; i < 8; ++i) {
					game_selector.set_pos(game_selector.get_pos_x() - 4, game_selector.get_pos_y());
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(sector.select_x < 15) {
				++sector.select_x;
				for(i <- 0; i < 8; ++i) {
					game_selector.set_pos(game_selector.get_pos_x() + 4, game_selector.get_pos_y());
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(sector.select_y) {
				--sector.select_y;
				for(i <- 0; i < 8; ++i) {
					game_selector.set_pos(game_selector.get_pos_x(), game_selector.get_pos_y() - 4);
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(sector.select_y < 15) {
				++sector.select_y;
				for(i <- 0; i < 8; ++i) {
					game_selector.set_pos(game_selector.get_pos_x(), game_selector.get_pos_y() + 4);
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("menu-select") || sector.Tux.get_input_held("jump")) {
			new_orientation <- 0;
			if(sector.gameboard[sector.select_x][sector.select_y] & 8) new_orientation = new_orientation | 1;
			if(sector.gameboard[sector.select_x][sector.select_y] & 4) new_orientation = new_orientation | 2;
			if(sector.gameboard[sector.select_x][sector.select_y] & 2) new_orientation = new_orientation | 8;
			if(sector.gameboard[sector.select_x][sector.select_y] & 1) new_orientation = new_orientation | 4;
			sector.gameboard[sector.select_x][sector.select_y] = new_orientation;
			sector.game_tiles[sector.select_x][sector.select_y].set_visible(false);
			sector.game_tiles[sector.select_x][sector.select_y] = FloatingImage("/levels/quiddlesworth/images/pipes/" + new_orientation + ".png");
			sector.game_tiles[sector.select_x][sector.select_y].set_pos(-240 + 32 * sector.select_x, -240 + 32 * sector.select_y);
			sector.game_tiles[sector.select_x][sector.select_y].set_layer(945);
			sector.game_tiles[sector.select_x][sector.select_y].set_visible(true);
			while(sector.Tux.get_input_held("menu-select") || sector.Tux.get_input_held("jump")) wait(0);
			if(check_win()) {
				win();
				return;
			}
		}
	}
}
