function shuffle() {
	for(i <- 0; i < 128; ++i) {
		for(x <- 0; x < 4; ++x) for(y <- 0; y < 4; ++y) {
			if(!sector.gameboard[x][y]) {
				sector.empty_hole <- [x, y];
				continue;
			}
		}
		move <- rand() & 3;
		if(!move) {
			if(sector.empty_hole[0] < 3) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0] + 1][sector.empty_hole[1]];
				sector.gameboard[sector.empty_hole[0] + 1][sector.empty_hole[1]] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0] + 1][sector.empty_hole[1]];
				sector.sliding_puzzle[sector.empty_hole[0] + 1][sector.empty_hole[1]] = null;
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x() - 128, sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y());
				++sector.empty_hole[0];
			}
		} else if(move == 1) {
			if(sector.empty_hole[0]) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0] - 1][sector.empty_hole[1]];
				sector.gameboard[sector.empty_hole[0] - 1][sector.empty_hole[1]] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0] - 1][sector.empty_hole[1]];
				sector.sliding_puzzle[sector.empty_hole[0] - 1][sector.empty_hole[1]] = null;
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x() + 128, sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y());
				--sector.empty_hole[0];
			}
		} else if(move == 2) {
			if(sector.empty_hole[1] < 3) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] + 1];
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] + 1] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] + 1];
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] + 1] = null;
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x(), sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y() - 128);
				++sector.empty_hole[1];
			}
		} else {
			if(sector.empty_hole[1]) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] - 1];
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] - 1] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] - 1];
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] - 1] = null;
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x(), sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y() + 128);
				--sector.empty_hole[1];
			}
		}
	}
}

function win() {
	sector.puzzle_completed <- true;
	Stage.spawn_item(topaz, sector.Tux.get_x(), sector.Tux.get_y() - 192);
	Stage.live_caption("Nice! It rewarded me with the gem from the puzzle!", "beige", "middle", 5);
}

if(!("sliding_puzzle" in sector)) {
	sector.puzzle_completed <- false;
	sector.sliding_puzzle <- array(4);
	for(i <- 0; i < 4; ++i) {
		sector.sliding_puzzle[i] = array(4);
		for(j <- 0; j < 4; ++j) sector.sliding_puzzle[i][j] = "" + i + j;
	}
	sector.sliding_puzzle[0][0] = null;
	sector.gameboard <- array(4);
	for(i <- 0; i < 4; ++i) {
		sector.gameboard[i] = array(4);
		for(j <- 0; j < 4; ++j) sector.gameboard[i][j] = null;
	}
	for(x <- 0; x < 4; ++x) for(y <- 0; y < 4; ++y) {
		if(!sector.sliding_puzzle[x][y]) continue;
		sector.gameboard[x][y] = FloatingImage("/levels/quiddlesworth/images/sliding_puzzle/" + sector.sliding_puzzle[x][y] + ".png");
		sector.gameboard[x][y].set_layer(950);
		sector.gameboard[x][y].set_pos(-192 + 128 * x, -192 + 128 * y);
	}
	shuffle();
}

sector.background <- FloatingImage("/levels/quiddlesworth/images/sliding_puzzle/background.png");
sector.background.set_layer(940);

sector.grid <- FloatingImage("/levels/quiddlesworth/images/sliding_puzzle/grid.png");
sector.grid.set_layer(960);

sector.infotext <- TextObject();
sector.infotext.set_text(_("Press ACTION to close"));
sector.infotext.set_anchor_point(ANCHOR_TOP_LEFT);
sector.infotext.set_text_color(1, 1, 0, 1);
sector.infotext.set_back_fill_color(0, 0, 0, 0);
sector.infotext.set_front_fill_color(0, 0, 0, 0);
sector.infotext.set_pos(10, 18);

function solved() {
	for(test_x <- 0; test_x < 4; ++test_x) for(test_y <- 0; test_y < 4; ++test_y) {
		if(!test_x && !test_y) continue;
		if("" + test_x + test_y != sector.sliding_puzzle[test_x][test_y]) return false;
	}
	return true;
}

function close_board() {
	missing_piece <- FloatingImage("/levels/quiddlesworth/images/sliding_puzzle/00.png");
	missing_piece.set_pos(-192, -192);
	missing_piece.set_layer(950);
	missing_piece.fade_in(0.6);
	sector.grid.fade_out(0.3);
	play_sound("sounds/invincible_start.ogg");
	legs.fade(0, 0);
	thumb.set_visible(false);
	wait(2.5);

	sector.infotext.set_visible(false);
	sector.background.set_visible(false);
	sector.grid.set_visible(false);
	for(x <- 0; x < 4; ++x) for(y <- 0; y < 4; ++y) if(sector.gameboard[x][y]) gameboard[x][y].set_visible(false);
	missing_piece.set_visible(false);
	Stage.dialogue_open <- false;
	sector.Tux.activate();
}

function start_puzzle() {
	sector.infotext.set_visible(true);
	Stage.dialogue_open <- true;
	sector.Tux.deactivate();
	sector.background.set_visible(true);
	sector.grid.set_visible(true);
	sector.empty_hole <- null;
	for(x <- 0; x < 4; ++x) for(y <- 0; y < 4; ++y) {
		if(!sector.gameboard[x][y]) {
			sector.empty_hole <- [x, y];
			continue;
		} else sector.gameboard[x][y].set_visible(true);
	}
	while(sector.Tux.get_input_held("left") || sector.Tux.get_input_held("right") || sector.Tux.get_input_held("up") || sector.Tux.get_input_held("down")) wait(0.01);
	while(true) {
		wait(0.01);
		if(sector.Tux.get_input_held("action")) {
			sector.background.set_visible(false);
			sector.grid.set_visible(false);
			for(x <- 0; x < 4; ++x) for(y <- 0; y < 4; ++y) if(sector.gameboard[x][y]) sector.gameboard[x][y].set_visible(false);
			Stage.dialogue_open <- false;
			sector.Tux.activate();
			sector.infotext.set_visible(false);
			return;
		} else if(sector.Tux.get_input_held("left")) {
			if(sector.empty_hole[0] < 3) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0] + 1][sector.empty_hole[1]];
				sector.gameboard[sector.empty_hole[0] + 1][sector.empty_hole[1]] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0] + 1][sector.empty_hole[1]];
				sector.sliding_puzzle[sector.empty_hole[0] + 1][sector.empty_hole[1]] = null;
				for(i <- 0; i < 32; ++i) {
					sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x() - 4, sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y());
					wait(0.01);
				}
				++sector.empty_hole[0];
				if(solved()) {
					close_board();
					win();
					return;
				}
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(sector.empty_hole[0]) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0] - 1][sector.empty_hole[1]];
				sector.gameboard[sector.empty_hole[0] - 1][sector.empty_hole[1]] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0] - 1][sector.empty_hole[1]];
				sector.sliding_puzzle[sector.empty_hole[0] - 1][sector.empty_hole[1]] = null;
				for(i <- 0; i < 32; ++i) {
					sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x() + 4, sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y());
					wait(0.01);
				}
				--sector.empty_hole[0];
				if(solved()) {
					close_board();
					win();
					return;
				}
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(sector.empty_hole[1] < 3) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] + 1];
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] + 1] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] + 1];
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] + 1] = null;
				for(i <- 0; i < 32; ++i) {
					sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x(), sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y() - 4);
					wait(0.01);
				}
				++sector.empty_hole[1];
				if(solved()) {
					close_board();
					win();
					return;
				}
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(sector.empty_hole[1]) {
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]] = sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] - 1];
				sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1] - 1] = null;
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1]] = sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] - 1];
				sector.sliding_puzzle[sector.empty_hole[0]][sector.empty_hole[1] - 1] = null;
				for(i <- 0; i < 32; ++i) {
					sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].set_pos(sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_x(), sector.gameboard[sector.empty_hole[0]][sector.empty_hole[1]].get_pos_y() + 4);
					wait(0.01);
				}
				--sector.empty_hole[1];
				if(solved()) {
					close_board();
					win();
					return;
				}
			}
		}
	}
}
