pigeon0 <- [
	["[TUX] Hello. Where is everybody?", "beige"],
	["[PIGEON] Afternoon. Where do you think? Everyone packed up and flew away this morning. I would have too, if it weren't for my unfortunate broken wing.", "violet"],
	["[TUX] I'm sorry to hear about your wing. I'm Tux, I came here hoping I could help with the situation. Would you happen to know a way into the castle?", "beige"],
	["[PIGEON] Nice to meet you, Tux. My name is Tara. Unfortunately, I don't think there is a way in there. The front door is locked, all windows are closed, and noting seems to work. We tried picking the lock, smashing the windows, prying the door open, nothing worked. Then everyone just gave up and left me here.", "violet"],
	["[TUX] Ah. I was afraid it wouldn't be that easy. Either way, I will try my best. I firmly believe that there's always a way, even when things seem impossible.", "beige"],
	["[TARA] I believe in you, Tux. Noone who isn't flight-enabled has managed to get to this part of the island in centuries. You did. If anyone can get in, it's you.", "violet"],
	["[TARA] However, I am afraid I can't help you much with my broken wing. But if you need something, you can always ask.", "violet"],
	["[TUX] Thank you, Tara."]
];

pigeon1 <- [
	["[TUX] Tara, would you happen to have something like vaseline? I ran across some old wheels, and I need to get them to spin.", "beige"],
	["[TARA] No, best I can do for you is this bottle of cooking oil, will it help?", "violet"]
];

pigeon2 <- [
	["[TUX] That's perfect! Thank you so much!", "beige"],
	["[TARA] Not a problem.", "violet"]
];

function pigeon() {
	sector.Tux.deactivate();
	sector.Tux.set_dir(false);
	sector.Tux.walk(20);
	wait(0.1);
	sector.Tux.walk(0);
	sector.alpid <- false;
	if(!sector.pigeon_talked) {
		sector.pigeon_talked <- true;
		sector.alpid <- true;
		Stage.dialogue(pigeon0);
	}
	if(Level.oil_status == 1) {
		Level.oil_status = 2;
		sector.alpid <- true;
		Stage.dialogue(pigeon1);
		Stage.collect_item(oil);
		Stage.dialogue(pigeon2);
	}
	if(!sector.alpid) {
		Stage.live_caption("Sorry Tux, I can't help you much.", "violet", "left", 3.49);
	}
	sector.Tux.activate();
}
