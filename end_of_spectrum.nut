magnet_reaction <- function() {
	sector.magnet_used <- true;
	vozejk.goto_node(1);
	vozejk2.goto_node(1);
	Stage.clear_accepted_items();
	wait(0.6);
	sector.Camera.shake(0.1, 5, 7);
	Stage.live_caption("Well, that worked.", "beige", "middle", 2);
}

function close() {
	tile1.set_visible(false);
	tile2.set_visible(false);
	tile3.set_visible(false);
	tile4.set_visible(false);
	pointer.set_visible(false);
	hanoi_bg.set_visible(false);
	sector.infotext.set_visible(false);

	sector.Tux.activate();
	Stage.dialogue_open <- false;
}

if(!("hanoi" in sector)) {
	sector.hanoi <- array(3);
	for(i <- 0; i < 3; ++i) sector.hanoi[i] = array(4);
	sector.hanoi[0][0] = 1;
	sector.hanoi[0][1] = 2;
	sector.hanoi[0][2] = 3;
	sector.hanoi[0][3] = 4;
	for(i <- 1; i < 3; ++i) for(j <- 0; j < 4; ++j) sector.hanoi[i][j] = 0;

	sector.t1 <- [0, 0];
	sector.t2 <- [0, 1];
	sector.t3 <- [0, 2];
	sector.t4 <- [0, 3];

	sector.pointer_pos <- 0;

	sector.held <- 0;
	sector.puzzle_done <- false;
}

sector.infotext <- TextObject();
sector.infotext.set_text(_("Press ACTION to close"));
sector.infotext.set_anchor_point(ANCHOR_TOP_LEFT);
sector.infotext.set_text_color(1, 1, 0, 1);
sector.infotext.set_back_fill_color(0, 0, 0, 0);
sector.infotext.set_front_fill_color(0, 0, 0, 0);
sector.infotext.set_pos(10, 18);

function get_top_block(tow, replace = true) {
	if(sector.hanoi[tow][3]) {
		ret <- sector.hanoi[tow][3];
		if(replace) {
			sector.hanoi[tow][3] = 0;
		}
		return ret;
	} else if(sector.hanoi[tow][2]) {
		ret <- sector.hanoi[tow][2];
		if(replace) {
			sector.hanoi[tow][2] = 0;
		}
		return ret;
	} else if(sector.hanoi[tow][1]) {
		ret <- sector.hanoi[tow][1];
		if(replace) {
			sector.hanoi[tow][1] = 0;
		}
		return ret;
	} else if(sector.hanoi[tow][0]) {
		ret <- sector.hanoi[tow][0];
		if(replace) {
			sector.hanoi[tow][0] = 0;
		}
		return ret;
	} else return 0;
}

function place_block(block, tower) {
	for(i <- 0; i < 4; ++i) {
		if(!sector.hanoi[tower][i]) {
			sector.hanoi[tower][i] = block;
			sector["t" + block][0] = tower;
			sector["t" + block][1] = i;
			while(sector["tile" + block].get_pos_y() < 194 - 64 * i) {
				sector["tile" + block].set_pos(-126 + 126 * tower, sector["tile" + block].get_pos_y() + 14);
				wait(0.01);
			}
			sector["tile" + block].set_pos(-126 + 126 * tower, 210 - 64 * i);
			sector.held <- 0;
			return;
		}
	}
}

function check_win() {
	if(sector.t4[0] == 1 && sector.t4[1] == 3) return true;
	return false;
}

function win() {
	play_sound("sounds/invincible_start.ogg");
	sector.puzzle_done <- true;
	prizemg <- FloatingImage("/levels/quiddlesworth/images/hanoi/amethyst_big.png");
	prizemg.set_layer(963);
	prizemg.fade_in(1);
	han_thumb.set_visible(false);
	legs.fade(0, 0);
	wait(3);
	close();
	prizemg.set_visible(false);
	Stage.spawn_item(amethyst, sector.Tux.get_x(), 224);
	Stage.live_caption("Awesome! I got an amethyst for that effort!", "beige", "middle", 4);
}

function play_hanoi() {
	sector.Tux.deactivate();
	Stage.dialogue_open <- true;

	hanoi_bg <- FloatingImage("/levels/quiddlesworth/images/hanoi/background.png");
	hanoi_bg.set_layer(940);
	hanoi_bg.set_visible(true);

	tile1 <- FloatingImage("/levels/quiddlesworth/images/hanoi/1.png");
	tile1.set_layer(950);
	tile2 <- FloatingImage("/levels/quiddlesworth/images/hanoi/2.png");
	tile2.set_layer(950);
	tile3 <- FloatingImage("/levels/quiddlesworth/images/hanoi/3.png");
	tile3.set_layer(950);
	tile4 <- FloatingImage("/levels/quiddlesworth/images/hanoi/4.png");
	tile4.set_layer(950);

	tile1.set_pos(-126 + 126 * sector.t1[0], 210 - 64 * sector.t1[1]);
	tile2.set_pos(-126 + 126 * sector.t2[0], 210 - 64 * sector.t2[1]);
	tile3.set_pos(-126 + 126 * sector.t3[0], 210 - 64 * sector.t3[1]);
	tile4.set_pos(-126 + 126 * sector.t4[0], 210 - 64 * sector.t4[1]);

	tile1.set_visible(true);
	tile2.set_visible(true);
	tile3.set_visible(true);
	tile4.set_visible(true);

	pointer <- FloatingImage("/levels/quiddlesworth/images/hanoi/pointer.png");
	pointer.set_layer(945);
	pointer.set_pos(-126 + 126 * sector.pointer_pos, -210);
	pointer.set_visible(true);

	sector.infotext.set_visible(true);

	if(sector.held) {
		sector["tile" + sector.held].set_pos(-126 + 126 * sector.pointer_pos, -210);
	}

	while(sector.Tux.get_input_held("up")) wait(0.01);

	while(true) {
		wait(0.01);
		if(sector.Tux.get_input_held("action")) {
			close();
			return;
		} else if(sector.Tux.get_input_held("left")) {
			if(sector.pointer_pos) {
				--sector.pointer_pos;
				for(i <- 0; i < 18; ++i) {
					pointer.set_pos(pointer.get_pos_x() - 7, -210);
					if(sector.held) sector["tile" + sector.held].set_pos(sector["tile" + sector.held].get_pos_x() - 7, -210);
					wait(0.01);
				}
			}
			if(sector.held) {
				sector["tile" + sector.held].set_pos(-126 + 126 * sector.pointer_pos, -210);
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(sector.pointer_pos < 2) {
				++sector.pointer_pos;
				for(i <- 0; i < 18; ++i) {
					pointer.set_pos(pointer.get_pos_x() + 7, -210);
					if(sector.held) sector["tile" + sector.held].set_pos(sector["tile" + sector.held].get_pos_x() + 7, -210);
					wait(0.01);
				}
			}
			if(sector.held) {
				sector["tile" + sector.held].set_pos(-126 + 126 * sector.pointer_pos, -210);
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(!sector.held) {
				sector.held <- get_top_block(sector.pointer_pos);
				if(sector.held) {
					while(sector["tile" + sector.held].get_pos_y() > -196) {
						sector["tile" + sector.held].set_pos(-126 + 126 * sector.pointer_pos, sector["tile" + sector.held].get_pos_y() - 14);
						wait(0.01);
					}
					sector["tile" + sector.held].set_pos(-126 + 126 * sector.pointer_pos, -210);
				}
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(sector.held && (!get_top_block(sector.pointer_pos, false) || get_top_block(sector.pointer_pos, false) < sector.held)) {
				place_block(sector.held, sector.pointer_pos);
				if(check_win()) {
					win();
					break;
				}
			}
		}
	}
}
