key_reaction <- function() {
	play_sound("sounds/turnkey.ogg");
	sector.Tux.deactivate();
	sector.disable_dex <- true;
	Stage.allow_inventory(false);
	wait(2);
	sector.settings.add_object("door", "dveretrun", 777, 609, "", "(script \"Stage.door(\\\"throne_room\\\", \\\"second_floor_corridor\\\")\")");
	sector.zamek.set_visible(false);
	sector.dvere.set_visible(false);
	sector.Tux.activate();
	Stage.allow_inventory(true);
}
