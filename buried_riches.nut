function rndir() {
	rn <- [[-2, 0], [2, 0], [0, -2], [0, 2]];
	for(i <- 0; i < 16; ++i) {
		for(j <- 0; j < 3; ++j) {
			if(rand() & 1) {
				temp <- rn[j];
				rn[j] = rn[j + 1];
				rn[j + 1] = temp;
			}
		}
	}
	return rn;
}

function is_passage(x, y) {
	if(x < 0 || y < 0 || x > 28 || y > 28) return false;
	return !ret[x][y];
}

function walled(x, y) {
	if(x && !ret[x - 1][y]) return false;
	if(x < 28 && !ret[x + 1][y]) return false;
	if(y && !ret[x][y - 1]) return false;
	if(y < 28 && !ret[x][y + 1]) return false;
	return true;
}

function pom(x, y) {
	if(!ret[x][y]) return true;
	if(x && !ret[x - 1][y]) return true;
	if(x < 28 && !ret[x + 1][y]) return true;
	if(y && !ret[x][y - 1]) return true;
	if(y < 28 && !ret[x][y + 1]) return true;
	return false;
}

function generate() {
	ret <- array(29);
	for(i <- 0; i < 29; ++i) {
		ret[i] = array(29);
		for(j <- 0; j < 29; ++j) ret[i][j] = true;
	}
	strtx <- rand();
	strty <- 2 + 2 * (strtx % 13);
	strtx <- 2 + 2 * ((strtx >> 8) % 13);
	ret[strtx][strty] = false;
	fc <- [[strtx - 2, strty], [strtx + 2, strty], [strtx, strty - 2], [strtx, strty + 2]];
	while(fc.len()) {
		ran_cell_index <- rand() % fc.len();
		ranx <- fc[ran_cell_index][0];
		rany <- fc[ran_cell_index][1];
		if(pom(ranx, rany)) {
			fc.remove(ran_cell_index);
			continue;
		}
		ret[ranx][rany] = false;

		order <- rndir();
		for(i <- 0; i < 4; ++i) {
			if(is_passage(ranx + order[i][0], rany + order[i][1])) {
				ret[ranx + order[i][0] / 2][rany + order[i][1] / 2] = false;
				break;
			}
		}
		if(ranx > 1 && ret[ranx - 2][rany] && walled(ranx - 2, rany)) fc.append([ranx - 2, rany]);
		if(ranx < 28 && ret[ranx + 2][rany] && walled(ranx + 2, rany)) fc.append([ranx + 2, rany]);
		if(rany > 1 && ret[ranx][rany - 2] && walled(ranx, rany - 2)) fc.append([ranx, rany - 2]);
		if(rany < 28 && ret[ranx][rany + 2] && walled(ranx, rany + 2)) fc.append([ranx, rany + 2]);
		fc.remove(ran_cell_index);
		if(fc.len() > 900) return ret;
	}
	return ret;
}

if(!("maze" in sector)) {
	sector.maze <- generate();
	sector.maze_finished <- false;
	sector.rat_x <- 0;
	sector.rat_y <- 0;
}

sector.infotext <- TextObject();
sector.infotext.set_text(_("Press ACTION to close"));
sector.infotext.set_anchor_point(ANCHOR_TOP_LEFT);
sector.infotext.set_text_color(1, 1, 0, 1);
sector.infotext.set_back_fill_color(0, 0, 0, 0);
sector.infotext.set_front_fill_color(0, 0, 0, 0);
sector.infotext.set_pos(10, 18);

function hide() {
	maze_bg.set_visible(false);
	for(i <- 0; i < 29; ++i) for(j <- 0; j < 29; ++j) if(maze_walls[i][j]) maze_walls[i][j].set_visible(false);
	maze_prize.set_visible(false);
	sector.infotext.set_visible(false);
	rat.set_visible(false);
	Stage.dialogue_open <- false;
	sector.Tux.activate();
}

function win() {
	winning_diamond <- FloatingImage("/levels/quiddlesworth/images/maze/diamond_big.png");
	winning_diamond.set_layer(970);
	winning_diamond.fade_in(1);
	sector.maze_finished <- true;
	play_sound("sounds/invincible_start.ogg");
	game_thumb.set_visible(false);
	thumb_tm.fade(0, 0);
	wait(2.5);
	sector.maze_finished <- true;
	hide();
	Stage.spawn_item(diamond, sector.Tux.get_x(), 448);
	Stage.live_caption("Great! I got a diamond for the maze!", "beige", "middle", 5);
	winning_diamond.set_visible(false);
}

function open_maze() {
	sector.Tux.deactivate();
	Stage.dialogue_open <- true;
	maze_bg <- FloatingImage("/levels/quiddlesworth/images/maze/background.png");
	maze_bg.set_layer(940);
	maze_bg.set_visible(true);
	maze_walls <- array(29);
	maze_prize <- FloatingImage("/levels/quiddlesworth/images/maze/diamond_tiny.png");
	maze_prize.set_layer(960);
	maze_prize.set_pos(224, 224);
	maze_prize.set_visible(true);
	sector.infotext.set_visible(true);
	for(i <- 0; i < 29; ++i) {
		maze_walls[i] = array(29);
		for(j <- 0; j < 29; ++j) maze_walls[i][j] = null;
	}
	for(x <- 0; x < 29; ++x) for(y <- 0; y < 29; ++y) {
		if(maze[x][y]) {
			maze_walls[x][y] = FloatingImage("/levels/quiddlesworth/images/maze/wall.png");
			maze_walls[x][y].set_layer(950);
			maze_walls[x][y].set_pos(-224 + 16 * x, -224 + 16 * y);
			maze_walls[x][y].set_visible(true);
		}
	}
	
	rat <- FloatingImage("/levels/quiddlesworth/images/maze/mouse.png");
	rat.set_layer(950);
	rat.set_pos(-224 + 16 * sector.rat_x, -224 + 16 * sector.rat_y);
	rat.set_visible(true);
	while(sector.Tux.get_input_held("up")) wait(0.01);
	while(true) {
		wait(0.01);
		if(sector.Tux.get_input_held("action")) {
			hide();
			return;
		} else if(sector.Tux.get_input_held("left")) {
			if(sector.rat_x && !maze[sector.rat_x - 1][sector.rat_y]) {
				--sector.rat_x;
				for(i <- 0; i < 8; ++i) {
					rat.set_pos(rat.get_pos_x() - 2, rat.get_pos_y());
					wait(0.01);
				}
				if(sector.rat_x == 28 && sector.rat_y == 28) {
					win();
					return;
				}
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(sector.rat_x < 28 && !maze[sector.rat_x + 1][sector.rat_y]) {
				++sector.rat_x;
				for(i <- 0; i < 8; ++i) {
					rat.set_pos(rat.get_pos_x() + 2, rat.get_pos_y());
					wait(0.01);
				}
				if(sector.rat_x == 28 && sector.rat_y == 28) {
					win();
					return;
				}
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(sector.rat_y && !maze[sector.rat_x][sector.rat_y - 1]) {
				--sector.rat_y;
				for(i <- 0; i < 8; ++i) {
					rat.set_pos(rat.get_pos_x(), rat.get_pos_y() - 2);
					wait(0.01);
				}
				if(sector.rat_x == 28 && sector.rat_y == 28) {
					win();
					return;
				}
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(sector.rat_y < 28 && !maze[sector.rat_x][sector.rat_y + 1]) {
				++sector.rat_y;
				for(i <- 0; i < 8; ++i) {
					rat.set_pos(rat.get_pos_x(), rat.get_pos_y() + 2);
					wait(0.01);
				}
				if(sector.rat_x == 28 && sector.rat_y == 28) {
					win();
					return;
				}
			}
		}
	}
}
