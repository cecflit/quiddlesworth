lead_key_reaction <- function() {
	play_sound("sounds/turnkey.ogg");
	sector.disable_dex <- true;
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	wait(2);
	sector.settings.add_object("door", "dverepostel", 777, 609, "", "(script \"Stage.door(\\\"royal_bedroom\\\", \\\"first_floor_corridor\\\")\")");
	sector.zamek.set_visible(false);
	sector.dvere.set_visible(false);
	sector.Tux.activate();
	Stage.allow_inventory(true);
}
