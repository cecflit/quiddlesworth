function cutscene() {
	sector.Tux.set_dir(true);
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	sector.Tux.walk(60);
	wait(2.61);
	sector.Tux.do_jump(-392);
	wait(0.62);
	sector.Tux.do_jump(-392);
	wait(0.78);
	sector.Tux.walk(0);
	Stage.live_caption("This must be the source of the curse! I wonder what this liquid is made of.", "beige", "middle", 6);
	wait(6);
	Stage.live_caption("You!", "violet", "left", 2);
	wait(0.25);
	sector.Tux.do_jump(-150);
	wait(0.1);
	sector.Tux.set_dir(false);
	wait(0.3);
	sector.Tux.walk(-120);
	while(sector.Camera.get_x() > 5 || sector.Tux.get_x() > 544) wait(0.1);
	sector.Tux.walk(0);
	Stage.live_caption("Gob! You were Bog all along!", "beige", "middle", 4.99);
	wait(4.7);
	Stage.live_caption("How observant of you, Tux. Tell me, how did you get here? I expected you to try to pass through the cave.", "violet", "left", 6.66);
	wait(6);

	Stage.live_caption("I did.", "beige", "middle", 1.5);
	wait(1.44);

	Stage.live_caption("Impossible, the exit is locked behind five gemstones, only three of which have ever been located.", "violet", "left", 6.66);
	wait(6);

	Stage.live_caption("I found all five. Two of them were submerged in flooded caverns.", "beige", "middle", 6.66);
	wait(6);

	Stage.live_caption("Ah, I see - penguin - swims. Never mind. Now that you are here, you may as well watch the show.", "violet", "left", 6.66);
	wait(6);

	Stage.live_caption("Show? What are you up to?", "beige", "middle", 3.8);
	wait(3.5);

	Stage.live_caption("You see, I spent the last few years of my life developing a fungus that would allow me to control others.", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("However, I needed a few test subjects before releasing it worldwide, so I fabricated the curse,", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("and nearly everybody believed it! You can't make this stuff up! So now, that they are afraid to leave", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("Quiddlesworth, I have them exactly where I need them. In a few hours, the fungus will grow to its full size", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("and release a powerful neurotoxin I designed. Then, I will be able to control everyone in Quiddlesworth!", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("*evil laugh*", "violet", "left", 3);
	wait(1.2);

	Stage.live_caption("I don't think so...", "beige", "middle", 3);
	wait(1.2);

	sector.Tux.set_dir(true);
	sector.Tux.walk(320);
	sector.doitnow <- true;

	Stage.live_caption("What are you doing?? STOP!", "violet", "left", 4);
	while(!("dojumpnow" in sector)) wait(0);

	sector.Tux.do_jump(-550);
	wait(0.84);
	sector.Tux.walk(0);
	flask.set_visible(true);
	Stage.steal_from_inventory("Weedkiller");
	Stage.update_status("Weedkiller used");
	sludge.set_action("pour");
	wait(3.3);
	sludge.set_action("empty");
	flask.enable_gravity(true);
	wait(0.5);
	Stage.live_caption("Wh... what?", "beige", "middle", 6.66);

	
	tree.goto_node(1);
	sector.Camera.shake(0.2, -15, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, -15);
	wait(0.2);
	sector.Camera.shake(0.2, 15, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, -15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 15, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, -15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 15, -15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, -15);
	wait(0.2);
	sector.Camera.shake(0.2, 15, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, -15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 15, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, -15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 15, -15);
	wait(0.2);

	sector.Tux.set_dir(false);
	sector.Tux.walk(-120);

	Stage.live_caption("HAHAHAHA! Is that herbicide I smell? Well, thank you, you have just sped up the process!", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("I bet the shopkeeper in Quiddlesworth gave you this idea...", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("You know, I accounted for every possible detail, so I gave those who didn't buy my ancient curse fairytale", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("a few false trails. I really am a genius.", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("Now, the process won't take much longer. I expect everybody to be my puppet in about two hours. Three if they get lucky.", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("As for you, penguin, I wholeheartedly thank you for speeding up the process, however, you are starting to annoy me.", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("I would kill you right now, but why deny myself the pleasure of controlling the mind of the one bird foolish enough to try to stop me?", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("*evil laugh*", "violet", "left", 6.66);
	wait(2);

	Stage.live_caption("You won't succeed, Gob. I'll make sure of that!", "beige", "middle", 6.66);
	wait(6);

	Stage.live_caption("I'd love to see you try, however, I have a lot of work to do.", "violet", "left", 6.66);
	wait(6);
	Effect.fade_out(1);
	wait(1);
	sector.Tux.activate();
	Stage.allow_inventory(true);
	Stage.save_inventory("level4");
	Stage.empty_inventory();
	Stage.teleport("cell", "throne_room");
}
