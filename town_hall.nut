mayor_dialogue0 <- [
	["[TUX] Good morning. I'm Tux, I arrived from Icy Island earlier. I would like to know whether I can help with the current events.", "beige"],
	["[MAYOR] Good morning, visitor. I am afraid that there is nothing you can do. I suggest you leave this island as soon as you can, for your own safety. I cannot help you.", "olive"],
	["[TUX] I see... Pity, I thought you may be able to help.", "beige"]
];

mayor_dialogue1 <- [
	["[MAYOR] Sorry, I cannot provide you with any help.", "olive"],
	["[TUX] Actually, I think you can. I've talked to Squeaky, and I am ready to face this curse.", "beige"],
	["[MAYOR] That is your decision to make, but I cannot give you any support.", "olive"],
	["[TUX] I'm not asking for any. I only need one thing from you... The source of the curse is on the other side of the island, and the only way there is through the cave. Mr. Mayor, I politely ask you, would you be willing to give me access to that cave?", "beige"],
	["[MAYOR] I see. But I will have to disappoint you. According to paragraph 8 of the municipal law, access to the cave shall only be granted to pre-approved expedition teams for a specified time. I cannot give you access unless you make a request as an expedition team and get it approved. That, however, is a process that takes months and you need to fill in all necessary paperwork.", "olive"],
	["[TUX] Surely there are mechanisms in the municipal law that allow for exceptions during emergencies?", "beige"],
	["[MAYOR] Hmm. I suppose that paragraph 87 says something about emergency decision making. But I will have to re-read it to make sure. However, my lunch break is about to start. If you want me to look at the law now, bring me a pack of acorns from the shop, and I might be willing to help you.", "olive"],
	["[TUX] Thank you, sir. I will be right back.", "beige"]
];

mayor_dialogue2 <- [
	["[TUX] Here are your acorns, Mr. Mayor.", "beige"],
	["[MAYOR] Thank you. I looked at the municipal law. Paragraph 87 does allow me to make exceptions to some rules, including access restrictions, during emergencies. However, to allow you access to the cave, you will have to sign a document stating that you are doing this at your own risk, and that no other parties bear any responsibility for any possible accidents.", "olive"],
	["[TUX] Fantastic! I will sign it.", "beige"],
	["[MAYOR] Okay then. Let me just write the document...", "olive"],
	["[MAYOR] Oh. We seem to be having a bit of an issue. I run out of ink. I am afraid you will have to get me some blue ink or a pen before I can write the document for you.", "olive"],
	["[TUX] Urgh. Fine, I'll get you some.", "beige"]
];

mayor_dialogue3 <- [
	["[TUX] I've got a pen for you.", "beige"],
	["[MAYOR] Excellent, let's write the document then, shall we?", "olive"],
	["[MAYOR] Wait, this pen has black ink. I cannot use that. Paragraph 22 of the municipal law states that all official documents must be penned in blue ink. Get me some blue ink instead.", "olive"],
	["[TUX] *sigh* Alright.", "beige"]
];

mayor_dialogue4 <- [
	["[TUX] I brought you some blue ink, Mr. Mayor.", "beige"],
	["[MAYOR] Let me see...\nHmm... a strange shade of blue, but why not. Let's write the document.", "olive"],
	["[MAYOR] *writing*", "olive"],
	["[MAYOR] There. Now sign here, please.", "olive"],
	["[TUX] Signed.", "beige"],
	["[MAYOR] Good. Now take this note to the theatre manager. He has the key to the cave for safekeeping, he will give it to you.", "olive"],
	["[TUX] Thank you, goodbye.", "beige"]
];

acorns_reaction <- function() {
	pack.set_visible(true);
	sector.Tux.set_dir(false);
	Stage.dialogue(mayor_dialogue2);
	Stage.add_accepted_item("Pen", pen_reaction, true);
	Stage.add_accepted_item("Blueberry juice", blueberry_juice_reaction);
	Level.ink_status <- 1;
}

pen_reaction <- function() {
	if(!("pen_tried" in sector)) {
		Stage.dialogue(mayor_dialogue3);
		sector.pen_tried <- true;
	} else {
		Stage.live_caption("I told you, all official documents must be written in blue ink.", "olive", "left", 3.33);
	}
}

blueberry_juice_reaction <- function() {
	Level.ink_status <- 3;
	Stage.dialogue(mayor_dialogue4);
	Stage.collect_item(mayornote);
}
