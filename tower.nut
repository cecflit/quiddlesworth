binoculars_reaction <- function() {
	Stage.inventory_selector.set_visible(false);
	Stage.inventory_info.set_visible(false);
	Stage.inventory_info2.set_visible(false);
	Stage.tip.set_visible(false);

	Stage.leave_sector();
	while(sector.Tux.get_input_held("menu-select")) wait(0.01);
	Level.spawn("observation", "main");
}
