function move(x, y, update = false) {
	sector.gameboard[x][y] = !sector.gameboard[x][y];
	if(update) {
		if(gameboard[x][y]) game_lights[x][y].fade_in(0.25);
		else game_lights[x][y].fade_out(0.25);
	}

	if(x) {
		sector.gameboard[x - 1][y] = !sector.gameboard[x - 1][y];
		if(update) {
			if(gameboard[x - 1][y]) game_lights[x - 1][y].fade_in(0.25);
			else game_lights[x - 1][y].fade_out(0.25);
		}
	}
	if(y) {
		sector.gameboard[x][y - 1] = !sector.gameboard[x][y - 1];
		if(update) {
			if(gameboard[x][y - 1]) game_lights[x][y - 1].fade_in(0.25);
			else game_lights[x][y - 1].fade_out(0.25);
		}
	}
	if(x < 4) {
		sector.gameboard[x + 1][y] = !sector.gameboard[x + 1][y];
		if(update) {
			if(gameboard[x + 1][y]) game_lights[x + 1][y].fade_in(0.25);
			else game_lights[x + 1][y].fade_out(0.25);
		}
	}
	if(y < 4) {
		sector.gameboard[x][y + 1] = !sector.gameboard[x][y + 1];
		if(update) {
			if(gameboard[x][y + 1]) game_lights[x][y + 1].fade_in(0.25);
			else game_lights[x][y + 1].fade_out(0.25);
		}
	}
}

if(!("gameboard" in sector)) {
	sector.gameboard <- array(5);
	for(i <- 0; i < 5; ++i) {
		sector.gameboard[i] = array(5);
		for(j <- 0; j < 5; ++j) sector.gameboard[i][j] = false;
	}
	for(i <- 0; i < 25; ++i) {
		rcoords <- rand();
		rx <- rcoords % 5;
		ry <- (rcoords >> 8) % 5;
		move(rx, ry);
	}
	sector.selector_x <- 2;
	sector.selector_y <- 2;

	sector.infotext <- TextObject();
	sector.infotext.set_text(_("Press ACTION to close"));
	sector.infotext.set_anchor_point(ANCHOR_TOP_LEFT);
	sector.infotext.set_text_color(1, 1, 0, 1);
	sector.infotext.set_back_fill_color(0, 0, 0, 0);
	sector.infotext.set_front_fill_color(0, 0, 0, 0);
	sector.infotext.set_pos(10, 18);
	sector.puzzle_finished <- false;
	sector.hcycle <- 0;
}

function hide() {
	sector.infotext.set_visible(false);
	game_bg.set_visible(false);
	selim.set_visible(false);
	for(i <- 0; i < 5; ++i) for(j <- 0; j < 5; ++j) game_lights[i][j].set_visible(false);
}

function won() {
	for(i <- 0; i < 5; ++i) for(j <- 0; j < 5; ++j) if(gameboard[i][j]) return false;
	return true;
}

function win() {
	play_sound("sounds/invincible_start.ogg");
	sector.puzzle_finished <- true;
	selim.set_visible(false);
	legs.fade(0, 0);
	thumbso.set_visible(false);
	wait(2.5);
	hide();
	sector.Tux.activate();
	Stage.dialogue_open <- false;
	Stage.live_caption("Fantastic! I won an emerald in this puzzle!", "beige", "middle", 3.75);
	Stage.spawn_item(emerald, sector.Tux.get_x(), sector.Tux.get_y() - 192);
	return;
}

function start() {
	sector.Tux.deactivate();
	Stage.dialogue_open <- true;

	game_bg <- FloatingImage("/levels/quiddlesworth/images/lightsoff/background.png");
	game_bg.set_layer(940);
	game_bg.set_visible(true);
	game_lights <- array(5);
	for(i <- 0; i < 5; ++i) {
		game_lights[i] = array(5);
		for(j <- 0; j < 5; ++j) {
			game_lights[i][j] = FloatingImage("/levels/quiddlesworth/images/lightsoff/tile.png");
			game_lights[i][j].set_layer(950);
			game_lights[i][j].set_pos(-204 + i * 102, -204 + j * 102);
		}
	}
	selim <- FloatingImage("/levels/quiddlesworth/images/lightsoff/selector.png");
	selim.set_pos(-204 + sector.selector_x * 102, -204 + sector.selector_y * 102);
	selim.set_layer(960);
	selim.set_visible(true);
	sector.infotext.set_visible(true);
	for(i <- 0; i < 5; ++i) for(j <- 0; j < 5; ++j) if(gameboard[i][j]) game_lights[i][j].fade_in(0.25);
	while(sector.Tux.get_input_held("up")) wait(0.01);

	sector.hcycle <- 0;
	lih <- "";
	while(true) {
		while(sector.Tux.get_input_held("menu-select") || (sector.Tux.get_input_held("jump") && !sector.Tux.get_input_held("up"))) wait(0.01);
		wait(0.01);

		if(sector.Tux.get_input_held("action")) {
			hide();
			sector.Tux.activate();
			Stage.dialogue_open <- false;
			return;
		} else if(sector.Tux.get_input_held("menu-select") || (sector.Tux.get_input_held("jump") && !sector.Tux.get_input_held("up"))) {
			move(sector.selector_x, sector.selector_y, true);
			sector.hcycle <- 0;
			if(won()) {
				win();
				return;
			}
		} else if(sector.Tux.get_input_held("left")) {
			if(lih != "left") {
				sector.hcycle <- 0;
				lih <- "left";
			}
			if(sector.selector_x) {
				--sector.selector_x;
				for(i <- 0; i < 6; ++i) {
					selim.set_pos(selim.get_pos_x() - 17, selim.get_pos_y());
					wait(0.01);
				}
				while(sector.Tux.get_input_held("left")) {
					wait(0.01);
					if(++sector.hcycle >= 40) break;
				}
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(lih != "right") {
				sector.hcycle <- 0;
				lih <- "right";
			}
			if(sector.selector_x < 4) {
				++sector.selector_x;
				for(i <- 0; i < 6; ++i) {
					selim.set_pos(selim.get_pos_x() + 17, selim.get_pos_y());
					wait(0.01);
				}
				while(sector.Tux.get_input_held("right")) {
					wait(0.01);
					if(++sector.hcycle >= 40) break;
				}
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(lih != "up") {
				sector.hcycle <- 0;
				lih <- "up";
			}
			if(sector.selector_y) {
				--sector.selector_y;
				for(i <- 0; i < 6; ++i) {
					selim.set_pos(selim.get_pos_x(), selim.get_pos_y() - 17);
					wait(0.01);
				}
				while(sector.Tux.get_input_held("up")) {
					wait(0.01);
					if(++sector.hcycle >= 40) break;
				}
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(lih != "down") {
				sector.hcycle <- 0;
				lih <- "down";
			}
			if(sector.selector_y < 4) {
				++sector.selector_y;
				for(i <- 0; i < 6; ++i) {
					selim.set_pos(selim.get_pos_x(), selim.get_pos_y() + 17);
					wait(0.01);
				}
				while(sector.Tux.get_input_held("down")) {
					wait(0.01);
					if(++sector.hcycle >= 40) break;
				}
			}
		} else {
			sector.hcycle <- 0;
		}
	}
}
