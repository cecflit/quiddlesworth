if(!("gate_open" in sector)) {
	sector.gate_open <- false;
	sector.ruby_placed <- false;
	sector.topaz_placed <- false;
	sector.emerald_placed <- false;
	sector.diamond_placed <- false;
	sector.amethyst_placed <- false;
}

function check() {
	return sector.ruby_placed && sector.topaz_placed && sector.emerald_placed && sector.diamond_placed && sector.amethyst_placed;
}

function win() {
	sector.gate_open <- true;
	Stage.dialogue_open <- true;
	sector.Tux.deactivate();
	sector.Tux.set_dir(true);
	sector.Tux.walk(100);
}

function boom() {
	sector.Tux.walk(0);
	wait(0.5);
	sector.settings.add_object("mrbomb", "", 1296, 704, "", "");
	sector.settings.add_object("mrbomb", "", 1296, 768, "", "");
	sector.settings.add_object("mrbomb", "", 1296, 832, "", "");
	sector.Tux.set_velocity(-50, -200);
	wait(0.3);
	sector.Tux.set_velocity(0, sector.Tux.get_velocity_y());
	wait(1);
	sector.Tux.walk(100);
	wait(2);
	Effect.fade_out(7);
	Stage.save_inventory("level2");
	wait(7.65);
	Level.finish(true);
}

ruby_reaction <- function() {
	ruby_so.set_visible(true);
	ruby_light.fade(1, 0.2);
	play_sound("sounds/flop.ogg");
	sector.ruby_placed <- true;
	if(check()) win();
}

topaz_reaction <- function() {
	topaz_so.set_visible(true);
	topaz_light.fade(1, 0.2);
	sector.topaz_placed <- true;
	play_sound("sounds/flop.ogg");
	if(check()) win();
}

emerald_reaction <- function() {
	emerald_so.set_visible(true);
	emerald_light.fade(1, 0.2);
	play_sound("sounds/flop.ogg");
	sector.emerald_placed <- true;
	if(check()) win();
}

diamond_reaction <- function() {
	diamond_so.set_visible(true);
	diamond_light.fade(1, 0.2);
	play_sound("sounds/flop.ogg");
	sector.diamond_placed <- true;
	if(check()) win();
}

amethyst_reaction <- function() {
	amethyst_so.set_visible(true);
	amethyst_light.fade(1, 0.2);
	play_sound("sounds/flop.ogg");
	sector.amethyst_placed <- true;
	if(check()) win();
}
