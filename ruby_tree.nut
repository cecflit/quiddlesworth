if(!("gameboard" in sector)) {
	sector.gameboard <- array(6);
	for(i <- 0; i < 6; ++i) {
		sector.gameboard[i] = array(6);
		for(j <- 0; j < 6; ++j) sector.gameboard[i][j] = 6 * (i % 3) + j + 1;
	}
	for(i <- 0; i < 256; ++i) {
		sw_x0 <- rand();
		sw_y0 <- sw_x0 % 6;
		sw_x1 <- (sw_x0 >> 8) % 6;
		sw_y1 <- (sw_x0 >> 16) % 6;
		sw_x0 <- (sw_x0 >> 24) % 6;

		sw_c <- sector.gameboard[sw_x0][sw_y0];
		sector.gameboard[sw_x0][sw_y0] = sector.gameboard[sw_x1][sw_y1];
		sector.gameboard[sw_x1][sw_y1] = sw_c;
	}
	sector.select_x <- 0;
	sector.select_y <- 0;
	sector.game_done <- false;

	sector.turned_cards <- [];
}

sector.infotext <- TextObject();
sector.infotext.set_text(_("Press ACTION to close"));
sector.infotext.set_anchor_point(ANCHOR_TOP_LEFT);
sector.infotext.set_text_color(1, 1, 0, 1);
sector.infotext.set_back_fill_color(0, 0, 0, 0);
sector.infotext.set_front_fill_color(0, 0, 0, 0);
sector.infotext.set_pos(10, 18);

function hide() {
	sector.infotext.set_visible(false);
	for(i <- 0; i < 6; ++i) for(j <- 0; j < 6; ++j) {
		game_backs[i][j].set_visible(false);
		game_fronts[i][j].set_visible(false);
	}
	game_selector.set_visible(false);
	game_bg.set_visible(false);
	while(sector.Tux.get_input_held("action")) wait(0.01);
	sector.Tux.activate();
	Stage.allow_inventory(true);
}

function check_win() {
	for(i <- 0; i < 6; ++i) for(j <- 0; j < 6; ++j) if(sector.gameboard[i][j]) return false;
	return true;
}

function win() {
	play_sound("sounds/invincible_start.ogg");
	game_selector.fade_out(0.5);
	sector.game_done <- true;
	thumblol.set_visible(false);
	legs.fade(0, 0);
	wait(3.6);
	game_bg.set_visible(false);
	sector.infotext.set_visible(false);
	sector.Tux.activate();
	Stage.allow_inventory(true);
	Stage.live_caption("Brilliant! A ruby fell from the tree when I finished the puzzle!", "beige", "middle", 3.75);
	sector.Camera.shake(0.1, 5, 7);
	Stage.spawn_item(ruby, 832, 544);
	fruit.set_visible(false);
}

function play() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);

	game_bg <- FloatingImage("/levels/quiddlesworth/images/pexeso/background.png");
	game_bg.set_layer(940);
	game_bg.set_visible(true);

	game_fronts <- array(6);
	for(i <- 0; i < 6; ++i) {
		game_fronts[i] = array(6);
		for(j <- 0; j < 6; ++j) {
			if(sector.gameboard[i][j]) game_fronts[i][j] = FloatingImage("/levels/quiddlesworth/images/pexeso/" + sector.gameboard[i][j] + ".png");
			else game_fronts[i][j] = FloatingImage("/levels/quiddlesworth/images/pexeso/back.png");
			game_fronts[i][j].set_layer(945);
			game_fronts[i][j].set_pos(-212 + 85 * i, -212 + 85 * j);
			if(sector.gameboard[i][j]) game_fronts[i][j].set_visible(true);
		}
	}

	game_backs <- array(6);
	for(i <- 0; i < 6; ++i) {
		game_backs[i] = array(6);
		for(j <- 0; j < 6; ++j) {
			game_backs[i][j] = FloatingImage("/levels/quiddlesworth/images/pexeso/back.png");
			game_backs[i][j].set_layer(950);
			game_backs[i][j].set_pos(-212 + 85 * i, -212 + 85 * j);
			if(sector.gameboard[i][j]) game_backs[i][j].set_visible(true);
		}
	}

	game_selector <- FloatingImage("/levels/quiddlesworth/images/pexeso/selector.png");
	game_selector.set_layer(960);
	game_selector.set_pos(-212 + 85 * sector.select_x, -212 + 85 * sector.select_y);
	game_selector.set_visible(true);

	sector.infotext.set_visible(true);

	if(sector.turned_cards.len()) {
		foreach(coords in sector.turned_cards) {
			game_backs[coords[0]][coords[1]].set_visible(false);
		}
	}

	while(sector.Tux.get_input_held("up")) wait(0.01);

	while(true) {
		wait(0.01);
		if(sector.Tux.get_input_held("action")) {
			hide();
			return;
		} else if(sector.Tux.get_input_held("left")) {
			if(sector.select_x) {
				--sector.select_x;
				for(i <- 0; i < 21; ++i) {
					game_selector.set_pos(game_selector.get_pos_x() - 4, game_selector.get_pos_y());
					wait(0.01);
				}
				game_selector.set_pos(-212 + 85 * sector.select_x, -212 + 85 * sector.select_y);
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(sector.select_x < 5) {
				++sector.select_x;
				for(i <- 0; i < 21; ++i) {
					game_selector.set_pos(game_selector.get_pos_x() + 4, game_selector.get_pos_y());
					wait(0.01);
				}
				game_selector.set_pos(-212 + 85 * sector.select_x, -212 + 85 * sector.select_y);
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(sector.select_y) {
				--sector.select_y;
				for(i <- 0; i < 21; ++i) {
					game_selector.set_pos(game_selector.get_pos_x(), game_selector.get_pos_y() - 4);
					wait(0.01);
				}
				game_selector.set_pos(-212 + 85 * sector.select_x, -212 + 85 * sector.select_y);
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(sector.select_y < 5) {
				++sector.select_y;
				for(i <- 0; i < 21; ++i) {
					game_selector.set_pos(game_selector.get_pos_x(), game_selector.get_pos_y() + 4);
					wait(0.01);
				}
				game_selector.set_pos(-212 + 85 * sector.select_x, -212 + 85 * sector.select_y);
			}
		} else if(sector.Tux.get_input_held("menu-select") || sector.Tux.get_input_held("jump")) {
			if(sector.turned_cards.len() == 2) {
				game_backs[sector.turned_cards[0][0]][sector.turned_cards[0][1]].set_visible(true);
				game_backs[sector.turned_cards[1][0]][sector.turned_cards[1][1]].set_visible(true);
				sector.turned_cards.remove(1);
				sector.turned_cards.remove(0);
			}
			if(sector.gameboard[sector.select_x][sector.select_y] && (!sector.turned_cards.len() || (!(sector.turned_cards[0][0] == sector.select_x && sector.turned_cards[0][1] == sector.select_y)))) {
				sector.turned_cards.append([sector.select_x, sector.select_y]);
				game_backs[sector.select_x][sector.select_y].set_visible(false);
				if(sector.turned_cards.len() == 2) {
					if(sector.gameboard[sector.turned_cards[0][0]][sector.turned_cards[0][1]] == sector.gameboard[sector.turned_cards[1][0]][sector.turned_cards[1][1]]) {
						game_fronts[sector.turned_cards[0][0]][sector.turned_cards[0][1]].fade_out(1.5);
						game_fronts[sector.turned_cards[1][0]][sector.turned_cards[1][1]].fade_out(1.5);
						sector.gameboard[sector.turned_cards[0][0]][sector.turned_cards[0][1]] = 0;
						sector.gameboard[sector.turned_cards[1][0]][sector.turned_cards[1][1]] = 0;
						sector.turned_cards.remove(1);
						sector.turned_cards.remove(0);
						if(check_win()) {
							win();
							return;
						}
					}
				}
			}
			while(sector.Tux.get_input_held("menu-select") || sector.Tux.get_input_held("jump")) wait(0.01);
		}
	}
}
