actor_dialogue0 <- [
	["[ACTOR] I'm sorry, all shows have been cancelled today.", "gold"],
	["[TUX] Actually, I haven't come to watch a show, I was wondering whether I could help with the curse.", "beige"],
	["[ACTOR] Ah. I'm afraid I can't help you with that, I'm not local, I only work here. I arrived in the early morning just to find out what happened overnight, and now I can't leave because the ferry is gone.", "gold"],
	["[TUX] I'm sorry to hear that. Goodbye.", "beige"],
	["[ACTOR] Bye.", "gold"]
]

actor_dialogue1 <- [
	["[ACTOR] I'm sorry, all shows have been cancelled today.", "gold"],
	["[TUX] Actually, I haven't come for a show. I would like to speak to the manager.", "beige"],
	["[ACTOR] Oh, boss is upstairs, you can go see him.", "gold"],
	["[TUX] Thank you.", "beige"]
];

actor_dialogue2 <- [
	["[TUX] Excuse me, could you tell me where the manager is?", "beige"],
	["[ACTOR] Just upstairs.", "gold"],
	["[TUX] Thank you.", "beige"]
];

actor_dialogue3 <- [
	["[TUX] Excuse me, could you remind me where the manager is?", "beige"],
	["[ACTOR] Just upstairs.", "gold"],
	["[TUX] Thank you.", "beige"]
];

manager_dialogue0 <- [
	["[TUX] Good morning. I am Tux, from Icy Island. I came to Quiddlesworth hoping to help with the curse.", "beige"],
	["[MANAGER] Good morning, Tux. It is nice of you to come, but I am afraid there isn't much that can be done.", "crimson"],
	["[TUX] Surely there's something I can do?", "beige"],
	["[MANAGER] Maybe you should ask someone else. I'm not very familiar with the story of the curse.", "crimson"],
	["[TUX] Never mind then. Goodbye.", "beige"],
	["[MANAGER] Farewell.", "crimson"]
];

manager_dialogue1 <- [
	["[TUX] Excuse me, the mayor sent me here.", "beige"],
	["[MANAGER] Huh? And what does the mayor want?", "crimson"],
	["[TUX] He told me you have the key to the cave, Here, I have a signed note from him.", "beige"],
	["[MANAGER] Interesting... The mayor doesn't give the key to just anybody.", "crimson"],
	["[TUX] Well, he gave it to you, didn't he?", "beige"],
	["[MANAGER] Only for safekeeping, I'm not allowed in the cave. And he only gave it to me because there's a safe here in the theatre.", "crimson"],
	["[MANAGER] Either way, the signature looks genuine. I'll give you the key. Just make sure you talk to Gob before going to the cave - he was there many times with the expeditions, so he may be able to give you some advice. He's the squirrel with an eyepatch over his right eye. He lost it to a twig, can you imagine?", "crimson"],
	["[TUX] Thank you.", "beige"]
];

actor_first <- function() {
	if(Level.ink_status == 3) {
		Stage.dialogue(actor_dialogue1);
		sector.asked_for_manager <- true;
	}
	else Stage.dialogue(actor_dialogue0);
}

talk_manager <- function() {
	if(!Level.key_given || Level.ink_status < 3) {
		if(!("manager_talked" in sector)) {
			sector.Tux.set_dir(true);
			Stage.dialogue(manager_dialogue0);
			sector.manager_talked <- true;
		} else Stage.live_caption("Sorry, Tux. Try asking someone else.", "crimson", "middle", 2.66);
	} else if("manager_talked" in sector) Stage.live_caption("I have nothing else for you, Tux.", "crimson", "middle", 2.33);
	else Stage.live_caption("Sorry, I have nothing else for you.", "crimson", "middle", 2.33);
}

mayornote_reaction <- function() {
	sector.Tux.set_dir(true);
	Stage.dialogue(manager_dialogue1);
	Level.know_gob <- true;
	Stage.collect_item(key);
	Level.key_given <- true;
}
