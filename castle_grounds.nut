crowbar0_reaction <- function() {
	play_sound("/sounds/metal_hit.ogg");
	Stage.live_caption("The crowbar gave up before the door did.", "beige", "middle", 3.49);
	Stage.replace_in_inventory("Crowbar", hook);
	sector.crowbar_bent <- true;
	Stage.remove_accepted_item("Crowbar");
	Stage.remove_accepted_item("Сrowbar");
	Stage.add_prevent_drop_function("Crowbar", crowbar_reaction);
	Stage.add_prevent_drop_function("Сrowbar", crowbar_reaction);
}

crowbar1_reaction <- function() {
	play_sound("/sounds/metal_hit.ogg");
	Stage.live_caption("The crowbar gave up before the door did.", "beige", "middle", 3.49);
	Stage.replace_in_inventory("Сrowbar", hook);
	sector.crowbar_bent <- true;
	Stage.remove_accepted_item("Crowbar");
	Stage.remove_accepted_item("Сrowbar");
	Stage.add_prevent_drop_function("Crowbar", crowbar_reaction);
	Stage.add_prevent_drop_function("Сrowbar", crowbar_reaction);
}

crowbar_reaction <- function() {
	Stage.live_caption("It didn't work the first time, it wouldn't work the second time.", "beige", "middle", 4.8);
}

sandpaper_reaction <- function() {
	Stage.live_caption("By the time I'm done sanding this door into dust, the Sun will have absorbed the Earth.", "beige", "middle", 4.84);
}

hammer_reaction <- function() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	for(hit <- 0; hit < 5; ++hit) {
		play_sound("sounds/switch.ogg");
		wait(0.4);
	}
	sector.Tux.activate();
	Stage.allow_inventory(true);
	Stage.live_caption("I don't know what the glass is made of, but it won't break.", "beige", "middle", 3.5);
}

key_reaction <- function() {
	Stage.live_caption("This is not the right key for the lock.", "beige", "middle", 3.98);
}
