newspaper_item <- Stage.StageItem("Newspaper", "/levels/quiddlesworth/images/newspaper.png", "");
seasoning <- Stage.StageItem("Exotic seasoning", "/levels/quiddlesworth/images/seasoning.png",
                             "I got this seasoning from a neighbour. He brought it back from a trip to the Amazonian rainforest.");
breadcrumbs <- Stage.StageItem("Bread crumbs", "/levels/quiddlesworth/images/breadcrumbs.png",
                               "They look like regular bread crumbs, but I wouldn't trust them.");
seasoned_fish <- Stage.StageItem("Seasoned fish", "/levels/quiddlesworth/images/seasoned_fish.png",
                                 "It's still warm, and very spicy!");
cold_fish <- Stage.StageItem("Cold fish", "/levels/quiddlesworth/images/cooked_fish.png",
                             "Cooked, but it has gone cold by now.");
cold_seasoned_fish <- Stage.StageItem("Seasoned fish", "/levels/quiddlesworth/images/seasoned_fish.png",
                                      "A very spicy fish. It's gone cold by now though.");
old_raw_fish <- Stage.StageItem("Raw fish", "/levels/quiddlesworth/images/raw_fish.png",
                                "I caught it this morning.");
paper <- Stage.StageItem("Piece of paper", "/levels/quiddlesworth/images/paper.png",
                         "A blank sheet of paper");
pennynote <- Stage.StageItem("Note for Penny", "/images/objects/letter/letter-small.png",
                             "It reads:\n\nDear Penny, I went to Quiddlesworth. Don't worry about me, I will be OK.\n-Tux");
pen <- Stage.StageItem("Pen", "/levels/quiddlesworth/images/pen.png",
                       "A regular pen with black ink");
fertiliser <- Stage.StageItem("Fertiliser", "/levels/quiddlesworth/images/fertiliser.png",
                              "A small package of high-quality fertiliser");
notebook <- Stage.StageItem("Notebook", "/levels/quiddlesworth/images/notebook.png", "");
binoculars <- Stage.StageItem("Binoculars", "/levels/quiddlesworth/images/binoculars.png",
                              "They look almost unused. I wonder how old they are.");
weedkiller <- Stage.StageItem("Weedkiller", "/levels/quiddlesworth/images/weedkiller.png",
                              "Very potent, smells corrosive.");
acorns <- Stage.StageItem("Bag of acorns", "/levels/quiddlesworth/images/acorns.png",
                          "100% natural, eco-friendly acorns... plus a plastic bag.");
mayornote <- Stage.StageItem("Authorisation note", "/images/objects/letter/letter-small.png",
                             "It reads:\n\nI hereby authorise Tux of Icy Island to pick up the Key to the Cave and use it to gain access to the Cave\nSigned,\n-Mayor Bushtail");
blueberries <- Stage.StageItem("Blueberries", "/levels/quiddlesworth/images/blueberries.png",
                               "Very juicy, local fruit");
vial <- Stage.StageItem("Empty flask", "/levels/quiddlesworth/images/vial.png",
                        "No signs of what this flask was used for before. It's completely empty now.");
blueberry_juice <- Stage.StageItem("Blueberry juice", "/levels/quiddlesworth/images/blueberry_juice.png",
                                   "Freshly squeezed. It turns everything it touches blue.");
key <- Stage.StageItem("Key", "/levels/quiddlesworth/images/key.png",
                       "This is the key to the Quiddlesworth Cave. With it, I can now go there.");
key2 <- Stage.StageItem("Key", "/levels/quiddlesworth/images/key.png",
                        "An old key. I wonder what it unlocks.");
key3 <- Stage.StageItem("Key", "/levels/quiddlesworth/images/key.png",
                        "This key was well hidden behind the fireplace in the library. It must be for an important door.");
key4 <- Stage.StageItem("Lead key", "/levels/quiddlesworth/images/key.png",
                        "I made this key using a broken lead pipe and a piece of clay for a mould. I wonder whether it can actually open anything.");
rod <- Stage.StageItem("Metal rod", "/images/tiles/blocks/brick_piece12.png",
                       "This piece of metal was blocking a cave passage.");
rod2 <- Stage.StageItem("Metal rod", "/images/tiles/blocks/brick_piece12.png",
                        "A long piece of metal. It doesn't look very strong.");
horseshoe <- Stage.StageItem("Horseshoe", "/levels/quiddlesworth/images/horseshoe.png",
                             "They used to use these for horses that worked in the mines. I think it's made of neodymium.");
magnet <- Stage.StageItem("Magnet", "/levels/quiddlesworth/images/magnet.png",
                          "This used to be a horseshoe. Now it's a strong magnet.");
emerald <- Stage.StageItem("Emerald", "/levels/quiddlesworth/images/emerald.png",
                           "A valuable green gemstone");
topaz <- Stage.StageItem("Topaz", "/levels/quiddlesworth/images/topaz.png",
                           "A valuable yellow gemstone");
diamond <- Stage.StageItem("Diamond", "/levels/quiddlesworth/images/diamond.png",
                           "A valuable clear, blueish gemstone");
ruby <- Stage.StageItem("Ruby", "/levels/quiddlesworth/images/ruby.png",
                           "A valuable red gemstone");
amethyst <- Stage.StageItem("Amethyst", "/levels/quiddlesworth/images/amethyst.png",
                           "A valuable purple gemstone");
heavy_rock <- Stage.StageItem("Heavy rock", "/levels/quiddlesworth/images/heavyrock.png",
                              "This thing probably weighs more than me.");
rope <- Stage.StageItem("Piece of rope", "/levels/quiddlesworth/images/rope.png",
                        "Very strong, but not long enough to reach from the cliff all the way to Quiddlesworth.");
hookrope <- Stage.StageItem("Rope and hook", "/levels/quiddlesworth/images/rope_with_hook.png",
                            "A strong rope with a hook attached.");
hammer <- Stage.StageItem("Hammer", "/levels/quiddlesworth/images/hammer.png",
                          "A regular hammer I found in a shed in Squid-in-the-Woods.");
hammernail <- Stage.StageItem("Hammer and nail", "/levels/quiddlesworth/images/hammer_and_nail.png",
                              "A regular hammer and a long metal nail.");
hook <- Stage.StageItem("Bent crowbar", "/levels/quiddlesworth/images/bent_crowbar.png",
                        "It looks like a fishing hook, but much stronger.");
oil <- Stage.StageItem("Cooking oil", "/levels/quiddlesworth/images/oil.png",
                       "I think it's sunflower oil. It's great for frying food.");
stick <- Stage.StageItem("Stick", "/images/tiles/blocks/brick_piece6.png",
                         "A small piece of wood. Not nearly enough for a campfire.");
match <- Stage.StageItem("Match", "/levels/quiddlesworth/images/match.png",
                         "A wooden stick dipped in white phosphorus powder. It's easy to ignite but very toxic.");
gunpowder <- Stage.StageItem("Gunpowder", "/levels/quiddlesworth/images/gunpowder.png",
                             "I found this in a broken cannon. I think it's enough to blow a concrete wall apart.");
phosphorus <- Stage.StageItem("White phosphorus", "/levels/quiddlesworth/images/phosphorus.png",
                              "Extremely flammable and toxic, there is enough to kill a penguin. I have to be careful with it.");
crowbar0 <- Stage.StageItem("Crowbar", "/levels/quiddlesworth/images/crowbar.png",
                            "A long metal rod for prising things open.");
crowbar1 <- Stage.StageItem("Сrowbar", "/levels/quiddlesworth/images/crowbar.png",
                            "A long metal rod for prising things open.");
sandpaper <- Stage.StageItem("Sandpaper", "/levels/quiddlesworth/images/sandpaper.png",
                             "A piece of fine sandpaper. Useful for polishing wood or removing rust.");
rusty_key <- Stage.StageItem("Rusty key", "/levels/quiddlesworth/images/rusty_key.png",
                             "An old rusty key. I wonder what it unlocks.");
code_item <- Stage.StageItem("Code", "/levels/quiddlesworth/images/code_item.png", "");
lead_pipe <- Stage.StageItem("Lead pipe", "/levels/quiddlesworth/images/pipe.png",
                             "A broken piece of an old lead pipe. It has a low melting point but can be quite toxic.");
molten_lead <- Stage.StageItem("Molten lead", "/levels/quiddlesworth/images/molten_lead.png",
                               "It's dangerously hot and the fumes are toxic.");
lead <- Stage.StageItem("Lump of lead", "/levels/quiddlesworth/images/lead.png",
                        "A solidified piece of lead. It used to be a pipe.");
bent_rod <- Stage.StageItem("Bent metal rod", "/levels/quiddlesworth/images/bent_rod.png",
                            "A bent piece of some soft metal. Not particularly useful anymore.");
wire <- Stage.StageItem("Piece of wire", "/levels/quiddlesworth/images/wire.png",
                        "A thin piece of an old bed spring.");
mud <- Stage.StageItem("Clay", "/levels/quiddlesworth/images/mud.png",
                       "It's quite soft and feels high quality.");
mould <- Stage.StageItem("Mould", "/levels/quiddlesworth/images/mould.png",
                         "A piece of clay with a key-shaped imprint in it.");

newspaper_book <- [
	"Newspaper",
	[["Breaking news!\nMystery in Quiddlesworth!", "crimson"], "/images/particles/ghost1.png",
	"When the residents of the Quiddlesworth forest woke up this morning, a nasty surprise awaited them. Their forest has started turning " +
	"purple! Our reporters immediately went there to talk to the locals. Most are too terrified to talk, but from what we can gather, " +
	"such events were predicted centuries ago, and the predictions were not exactly nice on the outcome. However, experts say there is " +
	"nothing to fear, that all has a scientific explanation. Well, the locals don't seem convinced. We will bring you more news on this " +
	"subject in tomorrow's issue."],
	[["Kitten lost in scary woods", "lightbrown"], null,
	"In other news, there have been reports of meowing near the town of Whipcombe. We are pleased to say that rescuers have arrived in time " +
	"and have found a lost kitten, who is now safe and happy."],
	[["Water is wet", "lightblue"], "/images/particles/water_piece1-1.png",
	"As unbelievable as it seems, reputable scientists have recently discovered that water is wet! It has been reported that this shocking " +
	"revelation was hidden from the public, but now, we are happy to announce that we are the first publication to report on this. " +
	"We have asked the head of the Council of Scientists for comments, but she declined with the following words: 'Are you bananas? Go " +
	"do something useful!' SCANDALOUS!"],
	[["Recipe of the day", "violet"], "/images/objects/eat-me/platter.png",
	"Today's Top recipe is: FISH. To prepare this excellent meal, follow these simple instructions:\n\n-Catch a fish\n-Cook it\n" +
	"-Add seasoning of your choice\n-Enjoy!"]
];

school_notebook <- [
	"Notebook",
	[["Matemathiks", "brick"], null, "3 + 1 = 4\n10 ÷ 5 = 2\n 15×2 = I don't know :(\n11 - 5 = 7?\n5 + 3 = 8"],
	[["Englysh", "orange"], null, "Homework on page 5 of the book.\n\nIt are veri hard"]
];

Stage.add_examine_function("Newspaper", function() {Stage.book(newspaper_book)});
Stage.add_examine_function("Notebook", function() {Stage.book(school_notebook)});

Stage.add_prevent_drop_function("Weedkiller", function() {Stage.live_caption("I should keep this, the shopkeeper said I may be able to kill the cursed vegetation with it.", "beige", "middle", 4)});

Stage.add_recipe("Cooked fish", "Exotic seasoning", seasoned_fish);
Stage.add_recipe("Piece of paper", "Pen", pennynote);
Stage.add_recipe("Cold fish", "Exotic seasoning", cold_seasoned_fish);
Stage.add_recipe("Blueberries", "Empty flask", blueberry_juice);
Stage.add_recipe("Piece of rope", "Bent crowbar", hookrope);
Stage.add_recipe("Stick", "White phosphorus", match);
Stage.add_recipe("Sandpaper", "Rusty key", key2);
Stage.add_recipe("Mould", "Molten lead", key4);
Stage.add_craft_function("Seasoned fish", function() {Stage.collect_item(seasoning); Stage.update_status("Seasoned fish created")});
Stage.add_craft_function("Note for Penny", function() {Stage.collect_item(pen); Stage.update_status("Note for Penny written")});
Stage.add_craft_function("Match", function() {Stage.collect_item(phosphorus); Stage.update_status("Match created")});
Stage.add_craft_function("Key", function() {Stage.collect_item(sandpaper); Stage.update_status("Key polished")});

wrong_key <- function() {
	Stage.live_caption("This is not the right key for this lock.", "beige", "middle", 4.7);
}

function generate_digicode() {
	Level.digicode <- [0, 0, 0, 0];
	while(Level.digicode[0] == 0 && Level.digicode[1] == 0 && Level.digicode[1] == 0 && Level.digicode[1] == 0) {
		for(dgi <- 0; dgi < 4; ++dgi) Level.digicode[dgi] = rand() % 10;
	}
}

view_digicode <- function() {
	dgc_bg <- FloatingImage("/levels/quiddlesworth/images/digits/code_note.png");
	dgc_bg.set_layer(900);
	dgc_bg.set_visible(true);

	dgc0 <- FloatingImage("/levels/quiddlesworth/images/digits/" + Level.digicode[0] + ".png");
	dgc1 <- FloatingImage("/levels/quiddlesworth/images/digits/" + Level.digicode[1] + ".png");
	dgc2 <- FloatingImage("/levels/quiddlesworth/images/digits/" + Level.digicode[2] + ".png");
	dgc3 <- FloatingImage("/levels/quiddlesworth/images/digits/" + Level.digicode[3] + ".png");
	dgc0.set_layer(915);
	dgc1.set_layer(915);
	dgc2.set_layer(915);
	dgc3.set_layer(915);

	dgc0.set_pos(-111, 0);
	dgc1.set_pos(-37, 0);
	dgc2.set_pos(37, 0);
	dgc3.set_pos(111, 0);

	dgc0.set_visible(true);
	dgc1.set_visible(true);
	dgc2.set_visible(true);
	dgc3.set_visible(true);

	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	pclose <- TextObject();
	pclose.set_wrap_width(sector.Camera.get_screen_width() - 8);
	pclose.set_anchor_point(ANCHOR_TOP_LEFT);
	pclose.set_pos(8, 17);
	pclose.set_back_fill_color(0.2, 0.2, 0.2, 0);
	pclose.set_front_fill_color(0.2, 0.2, 0.2, 0);
	pclose.set_text_color(1, 1, 0, 1);
	pclose.set_text(_("Press UP or ACTION to continue"));
	pclose.set_visible(true);

	while(!sector.Tux.get_input_held("jump") && !sector.Tux.get_input_held("up") && !sector.Tux.get_input_held("action")) wait(0.01);

	dgc_bg.set_visible(false);
	dgc0.set_visible(false);
	dgc1.set_visible(false);
	dgc2.set_visible(false);
	dgc3.set_visible(false);
	pclose.set_visible(false);
	while(sector.Tux.get_input_held("jump") || sector.Tux.get_input_held("up") || sector.Tux.get_input_held("action")) wait(0.01);
	sector.Tux.activate();
	Stage.allow_inventory(true);
}

Stage.add_examine_function("Code", view_digicode);
