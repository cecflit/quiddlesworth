squeaky_dialogue0 <- [
	["[TUX] Good morning. My name is Tux, I'm from Icy Island. I came here to try to help with the current events. Is there anything you can help me with?", "beige"],
	["[ILL SQUIRREL] Hello Tux, welcome to Quiddlesworth! I am Squeaky. It is very brave of you to come here to do what others deem impossible. I would go myself, but I've got a bad flu and can barely sit up in the bed. However, there's absolutely no guarantee of success and the journey you want to undertake can be very, very dangerous.", "orange"],
	["[TUX] I know, and I am ready. I want to help as much as I can.", "beige"],
	["[SQUEAKY] I'm pleased to hear that there is someone like you, Tux. But, before I help you, I want to make sure that you know what you're about to face. Please, take my binoculars from the shelf downstairs, take them to the observation tower, and have a good look towards the east of the island, since that's where it all started and that's where the effects are, at least for now, the most severe. Come back after.", "orange"],
	["[TUX] Very well, I'll be right back.", "beige"]
]

squeaky_dialogue1 <- [
	["[TUX] I've been to the tower.", "beige"],
	["[SQUEAKY] So you've seen what we're dealing with?", "orange"],
	["[TUX] Yes. It's terrifying.", "beige"],
	["[SQUEAKY] And you still think you can handle it?", "orange"],
	["[TUX] I do. I will try my best.", "beige"],
	["[SQUEAKY] In that case, I'll tell you everything I know.", "orange"],
	["[SQUEAKY] First, you need to know that the curse started in or around the old castle on the east coast of the island. This is supposedly the castle in which Bog, the ancient dark wizard, lived. The only way to that side of the island is through the cave, the entrance to which is just a short walk from the town to the east.", "orange"],
	["[SQUEAKY] Second, the cave itself is supposedly full of dark magic, and the exit towards the east side of the island is sealed shut. To open it, you will need to explore the cave and find clues.", "orange"],
	["[SQUEAKY] Because of this supposed curse, the entrance to the cave is usually locked, unless there are planned expeditions. The mayor has the key, but you will have to convince him to give it to you - he's not easy to deal with.", "orange"],
	["[SQUEAKY] When you get out of the cave, you will have to traverse a mostly uncharted side of this island. There's a small hamlet there, but because of the lack of access, only a few flying birds live there. I don't know much about them, but I think they might be able to help you.", "orange"],
	["[SQUEAKY] After you manage to find the castle, you will need to get inside. Since I've never been there, I don't know how easy or hard that may be, but I think that whatever this supposed curse is, it must be coming from there.", "orange"],
	["[SQUEAKY] How you deal with it, I don't know. I imagine you will have to look for clues inside the castle, and potentially access a hidden or well protected room.", "orange"],
	["[TUX] That sounds hard, but doable. I think I can get there.", "beige"],
	["[TUX] Squeaky, you call the curse and the history around it \"supposed\". Do you not believe the story to be accurate?", "beige"],
	["[SQUEAKY] No, Tux, I do not. I believe that whoever is responsible isn't some mighty wizard from yesteryear, and they certainly don't use dark magic. I fully believe that they live right now, and use advanced science to achieve this chaos.", "orange"],
	["[TUX] *gulp* Well, I guess I'll find out soon enough.", "beige"],
	["[TUX] Thank you for the help, Squeaky. Get better soon.", "beige"],
	["[SQUEAKY] Thank you, friend, for your bravery. I believe in you. Good luck, you will need it.", "orange"]
];

squeaky_dialogue2 <- [
	["[TUX] Hello again, Squeaky.", "beige"],
	["[SQUEAKY] Is there something you need?", "orange"]
];

about_mayor_dialogue <- [
	["[SQUEAKY] The mayor is one of the best this town has ever had. Under his leadership, Quiddlesworth has turned from ruins to a functioning town. However, he does have one drawback - he always sticks to paragraphs and protocols and can be surprisingly stubborn. It's not easy to change his mind.", "orange"]
];

about_mayor <- function() {
	if(!("asked_mayor" in sector)) {
		Stage.dialogue([["[TUX] Can you tell me more about the mayor?", "beige"]]);
		sector.asked_mayor <- true;
	} else Stage.dialogue([["[TUX] Can you remind me about the mayor?", "beige"]]);
	Stage.dialogue(about_mayor_dialogue);
	Stage.menu(squeaky_menu);
}

about_cave_dialogue <- [
	["[SQUEAKY] The local cave has always been a bit of a mystery to many. It's full of glowing mushrooms that glow so brightly that greenery can thrive in there, there are some caverns in there that were built by someone aeons ago, and the whole place gives a sort of magical vibe. The eastern exit has been sealed for centuries, and the entrance, which is nearby, is locked most of the time, with exceptions only being made for planned expeditions. The mayor holds the key, but he won't give it up very easily.", "orange"]
]

about_cave <- function() {
	if(!("asked_cave" in sector)) {
		Stage.dialogue([["[TUX] Can you tell me more about the cave?", "beige"]]);
		sector.asked_cave <- true;
	} else Stage.dialogue([["[TUX] Can you remind me about the cave?", "beige"]]);
	Stage.dialogue(about_cave_dialogue);
	Stage.menu(squeaky_menu);
}

about_east_dialogue <- [
	["[SQUEAKY] The eastern side of the island is only accessible through the cave, since there are high cliffs along the shore. There is a small hamlet near the cave exit, but due to the area's lack of accessibility, it is only inhabited by flying birds. Nobody else has been there in centuries. Other than that, the only structure on that side of the island is the old medieval castle, which supposedly used to be the home of Bog, the ancient dark wizard.", "orange"]
];

about_east <- function() {
	if(!("asked_east" in sector)) {
		Stage.dialogue([["[TUX] Can you tell me more about the eastern side of the island?", "beige"]]);
		sector.asked_east <- true;
	} else Stage.dialogue([["[TUX] Can you remind me about the eastern side of the island?", "beige"]]);
	Stage.dialogue(about_east_dialogue);
	Stage.menu(squeaky_menu);
}

about_castle_dialogue <- [
	["[SQUEAKY] The castle is located on the eastern shore of this island. It was most likely built during the middle ages, but there are few historical records about it. Supposedly, the castle's last resident was the ancient dark wizard Bog, who cursed the entire island. This castle is most likely the source of the power that is now destroying the Quiddlesworth forest.", "orange"]
];

about_castle <- function() {
	if(!("asked_castle" in sector)) {
		Stage.dialogue([["[TUX] Can you tell me more about the castle?", "beige"]]);
		sector.asked_castle <- true;
	} else Stage.dialogue([["[TUX] Can you remind me about the castle?", "beige"]]);
	Stage.dialogue(about_castle_dialogue);
	Stage.menu(squeaky_menu);
}

squeaky_menu <- [
	"What do you want to ask?",
	["About the mayor", about_mayor],
	["About the cave", about_cave],
	["About the east side", about_east],
	["About the castle", about_castle],
	["Leave", function() {Stage.dialogue(["[TUX] That will be all. Thank you.", "beige"], ["[SQUEAKY] No problem.", "orange"]);}]
];
