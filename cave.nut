key_reaction <- function() {
	if(!Level.map_given) {
		Stage.live_caption("I think I should go talk to Gob first. The theatre manager said he could have some valuable advice.", "beige", "middle", 5);
	} else {
		sector.Tux.deactivate();
		Stage.allow_inventory(false);
		play_sound("sounds/turnkey.ogg");
		wait(2);
		Stage.steal_from_inventory("Key");
		sector.door.goto_node(1);
		lock.goto_node(1);
		wait(2);
		sector.Tux.walk(100);
		Effect.fade_out(7);
		wait(8);
		Stage.save_inventory("level1");
		Stage.allow_inventory(true);
		Level.finish(true);
	}
}
