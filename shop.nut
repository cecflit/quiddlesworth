dia1 <- [
	["[TUX] Good morning", "beige"],
	["[SHOPKEEPER] To you too, Tux. What do you need?", "lime"],
	["[TUX] Would you happen to have any fish for sale?", "beige"],
	["[SHOPKEEPER] *sigh* Sorry, no fish. The supplier is late. Again.", "lime"],
	["[TUX] Never mind. I also need something to write a note. A piece of paper and a pen or a pencil.", "beige"],
	["[SHOPKEEPER] I am afraid I will have to dissapoint you again. The school bought all of my writing supplies yesterday.", "lime"],
	["[TUX] Do you have anything at all?", "beige"],
	["[SHOPKEEPER] Yes, actually. I have a warehouse full of refrigerators that nobody seems to want.", "lime"],
	["[TUX] I wonder why...\nDo you have anything else?", "beige"],
	["[SHOPKEEPER] I also have a few sacks of fertiliser, but I am not sure how useful those are here in the cold.", "lime"],
	["[TUX] Actually, I think I'll have one of those. I'm heading to a much warmer place.", "beige"],
	["[SHOPKEEPER] Alright then. Here's your purchase. Thank you for the business, Tux.", "lime"],
	["[TUX] Thank you and goodbye.", "beige"]
];

dia2 <- [
	["[SHOPKEEPER] Do you need something else?", "lime"],
	["[TUX] No, thank you. I think I'll just browse."]
];

shopkeep_first <- function() {
	Stage.dialogue(dia1);
	pack.set_visible(false);
	Stage.collect_item(fertiliser);
	Stage.update_status("Fertiliser purchased");
}

shopkeep_second <- function() {
	Stage.dialogue(dia2);
}
