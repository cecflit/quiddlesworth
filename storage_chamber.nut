function spawn_antigrav_item(item, x, y) {
	sector.settings.add_object(
		"powerup", "", x, y, "",
		"(disable-physics #t)(sprite \"" + item.sprite +
		"\")(script \"sector.Stage.collect_item(sector.Stage.StageItem(\\\"" +
		item.name + "\\\", \\\"" + item.sprite + "\\\", \\\"" +
		escape(escape(item.description)) + "\\\"))\")"
	);
}
