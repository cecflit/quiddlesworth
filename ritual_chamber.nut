gunpowder_reaction <- function() {
	primed.set_visible(true);
	Stage.add_accepted_item("Match", match_reaction);
	sector.blowup_stage <- 1;
	play_sound("/sounds/pop.ogg");
	Stage.live_caption("Now I just need someting to light it up with.", "beige", "right", 4);
}

match_reaction <- function() {
	sector.blowup_stage <- 2;
	primed.set_visible(false);
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	sector.Tux.set_dir(true);
	sector.settings.add_object("mrbomb", "", 2160, 672, "", "");
	sector.settings.add_object("mrbomb", "", 2160, 704, "", "");
	sector.settings.add_object("mrbomb", "", 2160, 736, "", "");
	sector.settings.add_object("mrbomb", "", 2160, 768, "", "");
	sector.Tux.set_velocity(-120, -463);
	wait(1);
	Stage.live_caption("Wow! That explosion was way bigger than I expected. I should have used a lot less gunpowder.", "beige", "middle", 5.8);
	sector.Tux.activate();
	Stage.allow_inventory(true);
}
