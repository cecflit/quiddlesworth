function win() {
	sector.dig0.set_action("g" + Level.data.rng[0]);
	sector.dig1.set_action("g" + Level.data.rng[1]);
	sector.dig2.set_action("g" + Level.data.rng[2]);
	sector.dig3.set_action("g" + Level.data.rng[3]);
	sector.solved <- true;
	sector.pedestals.goto_node(1);
	play_sound("sounds/invincible_start.ogg");
}
