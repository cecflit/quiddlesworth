shop_dialogue0 <- [
	["[TUX] Good morning. My name's Tux, I am from Icy Island. I arrived earlier today, hoping I could perhaps help with the current events.", "beige"],
	["[SHOPKEEPER] Good morning, Tux. I appreciate your selflessness and willingness to help, but I don't think there's much that can be done. The curse comes from the east, but nobody has been to that side of the island in centuries - there is no telling what even is there.", "yellow"],
	["[TUX] There is always a catch. Either way, I will try my best. So, if you have anything useful I might not know about the curse, please tell me.", "beige"],
	["[SHOPKEEPER] Well, the curse's origins are unknown. There is an old wife's tale, but its about as realistic as fiction itself. What I do know is that the origin point is, for sure, the castle on the east coast of this island. I don't know how you could possibly get there, but if you do, I expect the source to be of biological nature, some sort of fungus or maybe a plant. I'm not an adventurer, I just run this shop, so there's not much I can do for you, but if you really are going to go attempt to go there, please, take some weedkiller from the shelf. If the source really is biological, it may help you get rid of it.", "yellow"],
	["[TUX] Thank you! That will be useful for sure.", "beige"]
];

shop_dialogue1 <- [
	["[SHOPKEEPER] Anything else you need?", "yellow"],
	["[TUX] No, thank you. I think I'll just browse.", "beige"]
];

shop_dialogue2 <- [
	["[TUX] I also need one more thing. Would you happen to have a pack of acorns for sale?", "beige"],
	["[SHOPKEEPER] But of course! There, on the shelf. And please, don't pay me. I should be paying you for your sacrifice.", "yellow"],
	["[TUX] Thank you.", "beige"]
];

shop_dialogue3 <- [
	["[TUX] Hello again. I will need one more thing. Would you happen to have a pack of acorns for sale?", "beige"],
	["[SHOPKEEPER] But of course! There, on the shelf. And please, don't pay me. I should be paying you for your sacrifice.", "yellow"],
	["[TUX] Thank you.", "beige"]
];

shop_dialogue4 <- [
	["[SHOPKEEPER] Anything else you need?", "yellow"],
	["[TUX] Would you happen to have some blue ink in stock?", "beige"],
	["[SHOPKEEPER] Sorry, I only have black, pink, orange, red and yellow.", "yellow"],
	["[TUX] Never mind.", "beige"]
]
