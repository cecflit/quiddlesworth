if(!("rocks_placed" in sector)) sector.rocks_placed <- 0;

rock_reaction <- function() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	sector.Tux.set_velocity(0, -230);
	wait(0.2);
	sector["rock" + sector.rocks_placed].set_visible(true);
	sector["rock" + sector.rocks_placed].set_solid(true);
	sector["rock" + sector.rocks_placed].enable_gravity(true);
	if(++sector.rocks_placed == 6) {
		counterweight.goto_node(1);
		sector.door.goto_node(1);
		sector.Tux.activate();
		Stage.allow_inventory(true);
		wait(0.65);
		sector.Camera.shake(0.1, 5, 7);
	} else {
		sector.Tux.activate();
		Stage.allow_inventory(true);
	}
}
