function animate() {
	pour.set_action("pour");
	wait(1);
	fertiwater.fade(1, 1);
	wait(2);
	fertiwater.fade(0.2 * sector.used_fert, 0.5);
	pour.set_action("empty");
}

function win() {
	pour.set_action("pour");
	wait(1);
	fertiwater.fade(1, 1);
	wait(2);
	fertiwater.fade(0.2 * sector.used_fert, 0.5);
	pour.set_action("empty");
	wait(1.2);
	sector.Camera.shake(0.2, -15, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, -15);
	wait(0.2);
	sector.Camera.shake(0.2, 15, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, -15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 0);
	Effect.fade_out(1);
	wait(0.2);
	sector.Camera.shake(0.2, 15, 15);
	wait(0.2);
	sector.Camera.shake(0.2, 0, -15);
	wait(0.2);
	sector.Camera.shake(0.2, -15, 0);
	wait(0.2);
	sector.Camera.shake(0.2, 0, 15);
	wait(0.2);
	Stage.teleport("outro", "main");
}

fertiliser_reaction <- function() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	if(!("used_fert" in sector)) {
		sector.used_fert <- 0;
		Stage.live_caption("If weedkiller strenghtens it, maybe fertiliser can weaken it?", "beige", "right", 4.99);
		wait(1);
	}
	if(++sector.used_fert < 5) animate();
	else win();
	sector.Tux.activate();
	Stage.allow_inventory(true);
}
