if(!("cellar_entered" in sector)) {
	sector.cellar_entered <- true;
	positions <- array(4);
	for(piss <- 0; piss < 4; ++piss) positions[piss] = array(4);
	for(piss <- 0; piss < 4; ++piss) for(pjss <- 0; pjss < 4; ++pjss) positions[piss][pjss] = 0;
	sector.puzzle_solved <- false;
	sector.sele_x <- 0;
	sector.sele_y <- 0;
	sector.key_found <- false;
}

function check_win() {
	digit0 <- 8 * positions[0][0] + 4 * positions[1][0] + 2 * positions[2][0] + positions[3][0];
	if(digit0 != Level.digicode[0]) return false;
	digit1 <- 8 * positions[0][1] + 4 * positions[1][1] + 2 * positions[2][1] + positions[3][1];
	if(digit1 != Level.digicode[1]) return false;
	digit2 <- 8 * positions[0][2] + 4 * positions[1][2] + 2 * positions[2][2] + positions[3][2];
	if(digit2 != Level.digicode[2]) return false;
	digit3 <- 8 * positions[0][3] + 4 * positions[1][3] + 2 * positions[2][3] + positions[3][3];
	if(digit3 != Level.digicode[3]) return false;
	return true;
}

function load_puzzle() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);

	bek <- FloatingImage("/levels/quiddlesworth/images/digicode/background.png");
	bek.set_layer(900);
	bek.set_visible(true);

	sele <- FloatingImage("/levels/quiddlesworth/images/digicode/selector.png");
	sele.set_pos(-47.5 - 95 + 95 * sector.sele_x, -47.5 - 95 + 95 * sector.sele_y);
	sele.set_layer(925);
	sele.set_visible(true);

	sector.switches <- array(4);
	for(piss <- 0; piss < 4; ++piss) {
		sector.switches[piss] = array(4);
		for(pjss <- 0; pjss < 4; ++pjss) {
			sector.switches[piss][pjss] = FloatingImage("/levels/quiddlesworth/images/digicode/button.png");
			sector.switches[piss][pjss].set_layer(910);
			sector.switches[piss][pjss].set_pos(-47.5 - 95 - 23 + 95 * piss + positions[piss][pjss] * 46, -47.5 - 95 + 95 * pjss);
			sector.switches[piss][pjss].set_visible(true);
		}
	}

	redka <- FloatingImage("/levels/quiddlesworth/images/digicode/led_red.png");
	redka.set_layer(935);
	redka.set_pos(0, 220);
	redka.set_visible(true);

	greenka <- FloatingImage("/levels/quiddlesworth/images/digicode/led_green.png");
	greenka.set_layer(935);
	greenka.set_pos(0, 220);

	pclose <- TextObject();
	pclose.set_wrap_width(sector.Camera.get_screen_width() - 8);
	pclose.set_anchor_point(ANCHOR_TOP_LEFT);
	pclose.set_pos(8, 17);
	pclose.set_back_fill_color(0.2, 0.2, 0.2, 0);
	pclose.set_front_fill_color(0.2, 0.2, 0.2, 0);
	pclose.set_text_color(1, 1, 0, 1);
	pclose.set_text(_("Press ACTION to close"));
	pclose.set_visible(true);

	while(sector.Tux.get_input_held("up")) wait(0);

	while(!sector.Tux.get_input_held("action")) {
		if(sector.Tux.get_input_held("left")) {
			if(sector.sele_x) {
				--sector.sele_x;
				while(sele.get_pos_x() > (-47.5 - 95 + sector.sele_x * 95)) {
					sele.set_pos(sele.get_pos_x() - 5, sele.get_pos_y());
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("right")) {
			if(sector.sele_x < 3) {
				++sector.sele_x;
				while(sele.get_pos_x() < (-47.5 - 95 + sector.sele_x * 95)) {
					sele.set_pos(sele.get_pos_x() + 5, sele.get_pos_y());
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("up")) {
			if(sector.sele_y) {
				--sector.sele_y;
				while(sele.get_pos_y() > (-47.5 - 95 + sector.sele_y * 95)) {
					sele.set_pos(sele.get_pos_x(), sele.get_pos_y() - 5);
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("down")) {
			if(sector.sele_y < 3) {
				++sector.sele_y;
				while(sele.get_pos_y() < (-47.5 - 95 + sector.sele_y * 95)) {
					sele.set_pos(sele.get_pos_x(), sele.get_pos_y() + 5);
					wait(0);
				}
			}
		} else if(sector.Tux.get_input_held("menu-select") || sector.Tux.get_input_held("jump")) {
			if(positions[sector.sele_x][sector.sele_y]) {
				for(swi <- 0; swi < 23; ++swi) {
					switches[sector.sele_x][sector.sele_y].set_pos(switches[sector.sele_x][sector.sele_y].get_pos_x() - 2, switches[sector.sele_x][sector.sele_y].get_pos_y());
					wait(0);
				}
				positions[sector.sele_x][sector.sele_y] = 0;
			} else {
				for(swi <- 0; swi < 23; ++swi) {
					switches[sector.sele_x][sector.sele_y].set_pos(switches[sector.sele_x][sector.sele_y].get_pos_x() + 2, switches[sector.sele_x][sector.sele_y].get_pos_y());
					wait(0);
				}
				positions[sector.sele_x][sector.sele_y] = 1;
			}
			if(check_win()) {
				play_sound("sounds/invincible_start.ogg");
				greenka.set_visible(true);
				sele.set_visible(false);
				wait(3.2);
				dvere.goto_node(1);
				sector.puzzle_solved <- true;
				Stage.live_caption("There seems to be even more to these dungeons. Maybe they are linked with the castle?", "beige", "middle", 5.2);
				break;
			}
		}
		wait(0.01);
	}

	sector.Tux.activate();
	Stage.allow_inventory(true);

	pclose.set_visible(false);
	redka.set_visible(false);
	greenka.set_visible(false);
	sele.set_visible(false);
	bek.set_visible(false);
	for(piss <- 0; piss < 4; ++piss) for(pjss <- 0; pjss < 4; ++pjss) sector.switches[piss][pjss].set_visible(false);

	while(sector.Tux.get_input_held("action")) wait(0);
}

crowbar_reaction <- function() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	play_sound("/sounds/brick.wav");
	wait(0.5);
	play_sound("/sounds/brick.wav");
	wait(0.5);
	play_sound("/sounds/brick.wav");
	crowwood.fade(0, 0.8);
	wait(0.8);
	sector.Tux.activate();
	Stage.allow_inventory(true);
	Stage.live_caption("Ha! There's a key in there.", "beige", "middle", 3);
	sector.key_found <- true;
	Stage.clear_accepted_items();
	Stage.remove_prevent_drop_function("Gunpowder");
	Stage.spawn_item(rusty_key, 416, 576);
}

gunpowder_reaction <- function() {
	Stage.live_caption("I can't blow it up, it could damage the items that may be inside.", "beige", "middle", 4.5);
}
