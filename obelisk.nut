rope_reaction <- function() {
	brick.fade(1, 0);
	vert.fade(1, 0);
	horiz.fade(1, 0);
	true_brick.fade(0, 0);
	play_sound("sounds/crystallo-pop.ogg");
	Stage.live_caption("With this setup, I might be able to lift the rock.", "beige", "middle", 3);
	sector.rope_placed <- true;
}

dullrope_reaction <- function() {
	Stage.live_caption("The rope is too thick to fit through this hole.", "beige", "middle", 2.98);
}

oil_reaction <- function() {
	Stage.allow_inventory(false);
	sector.Tux.deactivate();
	if(sector.Tux.get_x() < 944) {
		sector.Tux.set_dir(true);
		sector.Tux.walk(128);
		while(sector.Tux.get_x() < 944) wait(0);
	} else if(sector.Tux.get_x() > 944) {
		sector.Tux.set_dir(false);
		sector.Tux.walk(-128);
		while(sector.Tux.get_x() > 944) wait(0);
	}
	sector.Tux.set_dir(false);
	sector.Tux.set_velocity(-200, -700);
	wait(0.5);
	play_sound("sounds/pop.ogg");
	sector.Tux.set_velocity(0, sector.Tux.get_velocity_y());
	wait(1);
	sector.Tux.set_dir(true);
	sector.Tux.walk(128);
	while(sector.Tux.get_x() < 944) wait(0);
	sector.Tux.set_velocity(200, -700);
	wait(0.5);
	play_sound("sounds/pop.ogg");
	sector.Tux.set_velocity(0, sector.Tux.get_velocity_y());
	wait(1);
	Stage.live_caption("The wheels are now lubricated.", "beige", "middle", 2.5);
	sector.wheels_lubricated <- true;
	sector.Tux.activate();
	Stage.remove_accepted_item("Cooking oil");
	Stage.allow_inventory(true);
}

hammer_reaction <- function() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	sector.rope_secured <- true;
	Stage.replace_in_inventory("Hammer and nail", hammer);
	longnail.set_visible(true);
	for(yi <- 0; yi < 4; ++yi) {
		play_sound("sounds/metal_hit.ogg");
		wait(0.5);
	}
	Stage.live_caption("Now it should stay in place.", "beige", "middle", 2.49);
	Stage.allow_inventory(true);
	sector.Tux.activate();
}

key_reaction <- function() {
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	play_sound("/sounds/turnkey.ogg");
	sector.compartment_open <- false;
	for(pedi <- 0; pedi < 64; ++pedi) {
		pedestaldoor.set_pos(pedestaldoor.get_pos_x(), pedestaldoor.get_pos_y() - 1);
		wait(0.07);
	}
	sector.Tux.activate();
	Stage.allow_inventory(true);
	Stage.live_caption("There's a hidden compartment in this pillar!", "beige", "middle", 3.5);
}

rusty_reaction <- function() {
	Stage.live_caption("I think this is the right key, but it won't fit with all the rust.", "beige", "middle", 4.8);
}
