raw_fish <- Stage.StageItem("Raw fish", "/levels/quiddlesworth/images/raw_fish.png", "A freshly caught fish. Yummy.");

breadcrumbs_reaction <- function() {
	sector.fish_fed <- false;
	Stage.live_caption("I'll throw these into the pond.\nThey may lure the fish.", "beige", "middle", 5);
}

spawn_fish <- function() {
	sector.settings.add_object("fish-jumping", "", 1488, 576, "", "(dead-script \"Stage.spawn_item(raw_fish, 1488, 576)\")");
	sector.settings.add_object("fish-jumping", "", 1872, 464, "", "(dead-script \"Stage.spawn_item(raw_fish, 1872, 576)\")");
}
