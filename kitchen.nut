if(!("fireplace_lit" in sector)) sector.fireplace_lit <- false;

cooked_fish <- Stage.StageItem("Cooked fish", "/levels/quiddlesworth/images/cooked_fish.png", "Still warm and smells delicious.");

melt <- function() {
	Stage.replace_in_inventory("Lead pipe", molten_lead);
	Stage.replace_in_inventory("Lump of lead", molten_lead);
	Stage.update_status("Molten lead created");
	Stage.live_caption("*cough* *cough*", "beige", "middle", 3.2);
}

cook_fish <- function() {
	Stage.replace_in_inventory("Raw fish", cooked_fish);
	Stage.update_status("Fish cooked");
}

lightup <- function() {
	sector.settings.add_object("livefire_dormant", "vohen", 670, 665, "left", "");
	Stage.add_accepted_item("Lead pipe", melt, true);
	Stage.add_accepted_item("Lump of lead", melt, true);
	Stage.add_accepted_item("Raw fish", cook_fish, true);
	sector.fireplace_lit = true;
	Stage.live_caption("Nice and cosy.", "beige", "middle", 3);
}
