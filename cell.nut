function cutscene() {
	play_sound("sounds/locked.ogg");
	Stage.live_caption("I'm sure you won't mind waiting here. It's just for a few hours, I'll release you once I control you.", "violet", "left", 6.66);
	wait(6);
	Stage.live_caption("*evil laugh*", "violet", "left", 6.66);
	wait(2);

	Stage.live_caption("We'll see who laughs last, Gob.", "beige", "middle", 5);
	wait(1.5);
	for(bg <- 0; bg < 20; ++bg) {
		sector.bog.move(5, 0);
		wait(0.05);
	}
	sector.Tux.activate();
	Stage.allow_inventory(true);

	wait(2.5);

	if(Level.data.stolen_items) Stage.live_caption("He took all of my stuff. I have to find a way out of this cell.", "beige", "middle", 6.66);
	else Stage.live_caption("I have to find a way out of this cell.", "beige", "middle", 4.91);
}

wire_reaction <- function() {
	Stage.live_caption("Maybe I can pick the lock?", "beige", "middle", 2);
	sector.Tux.deactivate();
	Stage.allow_inventory(false);
	play_sound("sounds/turnkey.ogg");
	wait(2);
	sector.Tux.activate();
	Stage.allow_inventory(true);
	Stage.live_caption("Yes!", "beige", "middle", 1);
	dvere.set_visible(false);
	zamek.set_visible(false);
	sector.settings.add_object("door", "mrizelol", 764, 675, "", "(sprite \"/levels/quiddlesworth/images/bardoor/bardoor.sprite\") (script \"Stage.door(\\\"prison\\\", \\\"cell\\\")\")");
	Level.data.cell_unlocked <- true;
}

clay_reaction <- function() {
	Stage.live_caption("Now I have a key imprint in this piece of clay.", "beige", "middle", 4.8);
	Stage.replace_in_inventory("Clay", mould);
	play_sound("sounds/crystallo-pop.ogg");
}
