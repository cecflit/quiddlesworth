if(!("sailor_convinced" in sector)) sector.sailor_convinced <- false;

first_dialogue <- [
	["[TUX] Good morning, sir.", "beige"],
	["[SAILOR] Morning. Do you seek my services?", "teal"],
	["[TUX] Yes, actually. I need to get to Quiddlesworth.", "beige"],
	["[SAILOR] Quiddlesworth?! Have you not read today's paper yet?", "teal"],
	["[TUX] Yes, sir, I have, that's why I need to get there.", "beige"],
	["[SAILOR] Well, little adventurer. I am not going there. Anywhere else, yes. Quiddlesworth, never!", "teal"],
	["[TUX] Come on, be brave!", "beige"],
	["[SAILOR] Bravery is one thing, but this would be outright stupid. I am staying put.", "teal"]
];

offer_gold <- function() {
	if(!("offered_gold" in sector)) {
		Stage.dialogue(["[TUX] I can pay you in gold.", "beige"], ["[SAILOR] I've got more gold than you'll ever see.", "teal"]);
		sector.offered_gold <- true;
	}
	else Stage.dialogue(["[TUX] Maybe you'll take the gold after all?", "beige"], ["[SAILOR] No can do. No chance.", "teal"]);
	Stage.menu(convince_menu);
}

offer_adverts <- function() {
	if(!("offered_adverts" in sector)) {
		Stage.dialogue(
			["[TUX] I can tell everybody in the village and beyond about your wonderful services. If you take me...", "beige"],
			["[SAILOR] I get enough business as is. Besides, it's not like they don't all know me already.", "teal"]
		);
		sector.offered_adverts <- true;
	} else Stage.dialogue(
			["[TUX] Maybe you will reconsider my advertising offer?", "beige"],
			["[SAILOR] And maybe I won't.", "teal"]
		);
	Stage.menu(convince_menu);
}

offer_food <- function() {
	if(!("offered_food" in sector)) {
		Stage.dialogue(
			["[TUX] Maybe I can convince you with some food? Fish, perhaps?", "beige"],
			["[SAILOR] No dice. My wife makes me more than enough every day.", "teal"]
		);
		sector.offered_food <- true;
	} else if("offered_gold" in sector && "offered_adverts" in sector && "suggested_swim" in sector && "suggested_competition" in sector) {
		Stage.dialogue(
			["[TUX] What if I give you some really, really good food?", "beige"],
			["[SAILOR] You know what? Fine. If you get me the best food my mouth has ever tasted, I'll take you to Quiddlesworth.", "teal"],
			["[TUX] Rest assured, Sir, I will bring you food so good that you will never forget it.", "beige"],
			["[SAILOR] Ha! I want to see that!", "teal"]
		);
		return;
	} else {
		Stage.dialogue(
			["[TUX] What if I give you some really, really good food?", "beige"],
			["[SAILOR] No food is good enough to make me go to Quiddlesworth.", "teal"]
		);
	}
	Stage.menu(convince_menu);
}

suggest_swim <- function() {
	if(!("suggested_swim" in sector)) {
		Stage.dialogue(
			["[TUX] Hmm. Well then, I think I may swim there on my own...", "beige"],
			["[SAILOR] There's no chance you can swim that far, penguin or not. But what do I care, I'm not your mother.", "teal"]
		);
		sector.suggested_swim <- true;
	} else {
		Stage.dialogue(
			["[TUX] I mean it, I will swim!", "beige"],
			["[SAILOR] Go for it, I take no responsibility.", "teal"]
		);
	}
	Stage.menu(convince_menu);
}

suggest_competition <- function() {
	if(!("suggested_competition" in sector)) {
		Stage.dialogue(
			["[TUX] You know, I could just go and find a different ferry. And you would lose out on business.", "beige"],
			["[SAILOR] Go for it, I get enough business even without going to cursed places.", "teal"]
		);
		sector.suggested_competition <- true;
	} else {
		Stage.dialogue(
			["[TUX] You know, I could also tell everyone else to use someone else's services.", "beige"],
			["[SAILOR] It's not like there's much competition here. They don't really have a choice, do they?", "teal"]
		);
	}
	Stage.menu(convince_menu);
}

convince_menu <- [
	"Convince him!",
	["Offer gold", offer_gold],
	["Offer advertisement", offer_adverts],
	["Offer food", offer_food],
	["Suggest you swim there", suggest_swim],
	["Suggest you use a different ferry", suggest_competition]
];

give_raw <- function() {
	Stage.dialogue(
		["[TUX] I brought you this freshly cought fish. It's a very special breed!", "beige"],
		["[SAILOR] Raw fish?!? Do you think I don't get enough of those sailing the seas? Keep it and try again!", "teal"]
	);
	Stage.collect_item(Stage.StageItem("Raw fish", "/levels/quiddlesworth/images/raw_fish.png", "A freshly caught fish. Apparently not good enough for the sailor."));
	Stage.update_status("Raw fish retained");
}

give_unseasoned <- function() {
	if(!("tried_unseason" in sector)) {
		Stage.dialogue(
			["[TUX] Sir, I've got this heavenly fish for you.", "beige"],
			["[SAILOR] Heavenly, you say? Let's see about that.", "teal"],
			["[TUX] I'm sure you'll enjoy it!", "beige"],
			["[SAILOR] Yuck! This fish tastes like air. I've never had anything this bland. Go try something different!", "teal"]
		);
		sector.tried_unseason <- true;
	} else {
		Stage.dialogue(
			["[TUX] Sir, I have a heavenly fish for you.", "beige"],
			["[SAILOR] You do? It looks just like the one you gave me before. I'm not taking it!", "teal"]
		);
		Stage.collect_item(Stage.StageItem("Cooked fish", "/levels/quiddlesworth/images/cooked_fish.png", "The sailor doesn't seem impressed with this fish. It's still warm though."));
		Stage.update_status("Cooked fish retained");
	}
}

give_seasoned <- function() {
	Stage.dialogue(
		["[TUX] Sir, I brought you a heavenly fish. Just for you!", "beige"],
		["[SAILOR] Heavenly eh? Well, I am getting quite hungry.", "teal"],
		["[TUX] So, what do you think?", "beige"],
		["[SAILOR] Yum. Wow. I don't know what you did to this fish, but it's divine! Come aboard, come aboard!", "teal"]
	);
	sector.sailor_convinced <- true;
	sector.Tux.deactivate();
	blockade.set_solid(false);
	sector.Tux.set_dir(true);
	sector.Tux.walk(60);
	wait(50);
}

sail_away <- function() {
	sector.Tux.walk(0);
	sector.settings.fade_to_ambient_light(0, 0, 0, 7);
	boat.goto_node(1);
	ferrysailor.goto_node(1);
	wait(8);
	Stage.save_inventory("level0");
	sector.Tux.set_bonus("grow");
	wait(0);
	Level.finish(true);
}

sailor <- function() {
	if(sector.sailor_convinced) {
		blockade.set_solid(false);
		return;
	}
	if(!("sailor_first_interaction" in sector)) {
		sector.sailor_first_interaction <- true;
		sector.Tux.deactivate();
		sector.Tux.set_velocity(-30, 0);
		wait(0.1);
		sector.Tux.set_velocity(0, 0);
		Stage.dialogue(first_dialogue);
		Stage.menu(convince_menu);
		sector.Tux.activate();
		Stage.add_accepted_item("Cooked fish", give_unseasoned);
		Stage.add_accepted_item("Seasoned fish", give_seasoned);
		Stage.add_accepted_item("Raw fish", give_raw);
	} else {
		sector.Tux.deactivate();
		sector.Tux.set_velocity(-30, 0);
		Stage.live_caption("Well, have you got the miracle food for me?", "teal", "right", 3.33);
		wait(0.25);
		if(!Stage.prevent_teleport) sector.Tux.activate();
	}
}
