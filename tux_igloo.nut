newspaper_dialogue <- [
	["[TUX] I'd better find out what this is all about. Quiddlesworth is not that far from here.", "beige"],
	["[TUX] There is a ferry nearby that can take me there.", "beige"]
];

if(!("newspaper_read" in sector)) Stage.add_collect_function("Newspaper", function() {
	Stage.book(newspaper_book);
	Stage.dialogue(newspaper_dialogue);
	sector.newspaper_read <- true;
	Stage.update_status("Newspaper" + " " + "collected")
	Stage.remove_collect_function("Newspaper");
});
